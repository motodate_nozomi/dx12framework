#pragma once

#include "BaseApp.h"
#include <ConstantBuffer.h>
#include <Material.h>

#pragma  comment(lib, "Framework.lib")

class SandboxApp : public BaseApp
{
public:
    SandboxApp(const uint32_t Width, const uint32_t Height);

    virtual ~SandboxApp();

private:
    const bool OnInitialize() override;

    void OnTerminate() override;

    void OnRender() override;
};