#include "VertexBuffer.h"
#include <Logger.h>

VertexBuffer::VertexBuffer()
{

}

VertexBuffer::~VertexBuffer()
{
    Terminate();
}

const bool VertexBuffer::Initialize(
    ID3D12Device* Device,
    size_t BufferSize,
    size_t Stride,
    const void* InitData)
{
    // 引数の有効性を検証
    if (!Device)
    {
        ELOG("VertexBuffer::Initialize >> Device が nullptrです.");
        return false;
    }

    if (BufferSize <= 0)
    {
        ELOG("VertexBuffer::Initialize >> BufferSize <= 0.");
        return false;
    }

    if (Stride <= 0)
    {
        ELOG("VertexBuffer::Initialize >> Stride <= 0.");
        return false;
    }

    // ヒープ設定
    D3D12_HEAP_PROPERTIES HeapProp = {};
    HeapProp.Type = D3D12_HEAP_TYPE_UPLOAD;
    HeapProp.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
    HeapProp.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
    HeapProp.CreationNodeMask = 1;
    HeapProp.VisibleNodeMask = 1;

    // リソース設定
    D3D12_RESOURCE_DESC ResrcDesc = {};
    ResrcDesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
    ResrcDesc.Alignment = 0;
    ResrcDesc.Width = UINT64(BufferSize);
    ResrcDesc.Height = 1;
    ResrcDesc.DepthOrArraySize = 1;
    ResrcDesc.MipLevels = 1;
    ResrcDesc.Format = DXGI_FORMAT_UNKNOWN;
    ResrcDesc.SampleDesc.Count = 1;
    ResrcDesc.SampleDesc.Quality = 0;
    ResrcDesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
    ResrcDesc.Flags = D3D12_RESOURCE_FLAG_NONE;

    // リソースを生成
    auto HR = Device->CreateCommittedResource(
        &HeapProp,
        D3D12_HEAP_FLAG_NONE,
        &ResrcDesc,
        D3D12_RESOURCE_STATE_GENERIC_READ,
        nullptr,
        IID_PPV_ARGS(Resource.GetAddressOf()));
    if (FAILED(HR))
    {
        ELOG("VertexBuffer::Initialize >> リソースの生成に失敗.");
        return false;
    }

    // 頂点バッファビューの設定
    View.BufferLocation = Resource->GetGPUVirtualAddress();
    View.StrideInBytes = UINT(Stride);
    View.SizeInBytes = UINT(BufferSize);

    // 初期化データ書き込み
    if (InitData)
    {
        void* MappedPtr = Map();
        if (!MappedPtr)
        {
            ELOG("VertexBuffer::Initialize >> 初期値の書き込みに失敗.");
            return false;
        }

        memcpy(MappedPtr, InitData, BufferSize);

        Unmap();
    }

    return true;
}

void VertexBuffer::Terminate()
{
    Resource.Reset();
    memset(&View, 0, sizeof(View));
}

void* VertexBuffer::Map()
{
    if (!Resource)
    {
        ELOG("VertexBuffer::Map >> リソースがnullptrです.");
        return nullptr;
    }

    void* MappedPtr = nullptr;
    auto HR = Resource->Map(0, nullptr, &MappedPtr);
    if (FAILED(HR))
    {
        ELOG("VertexBuffer::Map >> メモリマッピングに失敗.");
        return nullptr;
    }

    return MappedPtr;
}

void VertexBuffer::Unmap()
{
    if (Resource)
    {
        Resource->Unmap(0, nullptr);
    }
}

const D3D12_VERTEX_BUFFER_VIEW VertexBuffer::GetView() const
{
    return View;
}

