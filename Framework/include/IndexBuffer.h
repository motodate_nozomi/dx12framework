#pragma once

#include <d3d12.h>
#include <ComPtr.h>
#include <cstdint>

class IndexBuffer
{
public:
    IndexBuffer();
    ~IndexBuffer();

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="Device"></param>
    /// <param name="BufferSize"></param>
    /// <param name="InitData"></param>
    /// <returns></returns>
    const bool Initialize(
        ID3D12Device* const Device,
        size_t BufferSize,
        const uint32_t* InitData = nullptr
    );

    /// <summary>
    /// 破棄
    /// </summary>
    void Terminate();

    /// <summary>
    /// メモリマッピング
    /// </summary>
    /// <returns></returns>
    uint32_t* const Map();

    /// <summary>
    /// マッピング解除
    /// </summary>
    void Unmap();

    /// <summary>
    /// ビューを取得
    /// </summary>
    /// <returns></returns>
    const D3D12_INDEX_BUFFER_VIEW GetView() const;

private:
    // コピー禁止
    IndexBuffer(const IndexBuffer&) = delete;
    void operator=(const IndexBuffer&) = delete;

    ComPtr<ID3D12Resource> Resource = nullptr;
    D3D12_INDEX_BUFFER_VIEW View = {};
};