#pragma once

#include <d3d12.h>
#include <ComPtr.h>

class Fence
{
public:
    Fence();
    ~Fence();

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="Device"></param>
    /// <returns></returns>
    const bool Initialize(ID3D12Device* const Device);

    void Terminate();

    /// <summary>
    /// 指定時間待機する
    /// </summary>
    /// <param name="Queue"></param>
    /// <param name="Timeout"></param>
    void Wait(ID3D12CommandQueue* const Queue, UINT Timeout);

    /// <summary>
    /// シグナル状態になるまで待機する
    /// </summary>
    /// <param name="Queue"></param>
    void Sync(ID3D12CommandQueue* const Queue);

private:
    // コピー禁止
    Fence(const Fence&) = delete;
    void operator=(const Fence&) = delete;

    ComPtr<ID3D12Fence> D3D12Fence = nullptr;
    HANDLE Event = {};
    UINT Counter = 0;
};