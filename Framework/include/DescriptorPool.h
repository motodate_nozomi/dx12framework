#pragma once

#include <d3d12.h>
#include <atomic>
#include <ComPtr.h>
#include <Pool.h>

/// <summary>
/// ディスクリプタハンドル
/// ディスクリプタの位置を識別番号で持つ
/// </summary>
class DescriptorHandle
{
public:
    DescriptorHandle() = default;

    D3D12_CPU_DESCRIPTOR_HANDLE CPUHandle;
    D3D12_GPU_DESCRIPTOR_HANDLE GPUHandle;

    const bool HasCPU() const
    {
        return CPUHandle.ptr != 0;
    }

    const bool HasGPU() const
    {
        return GPUHandle.ptr != 0;
    }
};

class DescriptorPool
{
public:
    /// <summary>
    /// 生成処理
    /// </summary>
    /// <param name="Device"></param>
    /// <param name="Desc"></param>
    /// <param name="PoolPtr"></param>
    /// <returns></returns>
    static bool Create(
        ID3D12Device* Device,
        const D3D12_DESCRIPTOR_HEAP_DESC* Desc,
        DescriptorPool** PoolPtr
    );

    void AddRef();

    void Release();

    const uint32_t GetRefCount() const;

    /// <summary>
    /// ディスクリプタヒープの作成
    /// </summary>
    /// <returns></returns>
    DescriptorHandle* AllocHandle();

    void FreeHandle(DescriptorHandle*& HandlePtr);
    
    /// <summary>
    /// 利用可能ハンドル数
    /// </summary>
    /// <returns></returns>
    uint32_t GetAvailableHandleCount() const;

    /// <summary>
    /// 割り当て済のハンドル数
    /// </summary>
    /// <returns></returns>
    uint32_t GetAllocatedHandleCount() const;

    /// <summary>
    /// ハンドル総数
    /// </summary>
    /// <returns></returns>
    uint32_t GetHandleCount() const;

    ID3D12DescriptorHeap* const GetHeap() const;

private:
    DescriptorPool();
    ~DescriptorPool();
    DescriptorPool(const DescriptorPool&) = delete;
    void operator=(const DescriptorPool&) = delete;

    std::atomic<uint32_t> RefCount = 0;
    Pool<DescriptorHandle> HandlePool = {};
    ComPtr<ID3D12DescriptorHeap> Heap = nullptr;
    uint32_t DescriptorSize = 0;
};