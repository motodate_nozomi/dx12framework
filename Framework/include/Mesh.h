#pragma once

<<<<<<< HEAD
#include <ResMesh.h>
#include <VertexBuffer.h>
#include <IndexBuffer.h>

class Mesh
{
public:
    Mesh();
    ~Mesh();

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="Device"></param>
    /// <param name="Resource"></param>
    /// <returns></returns>
    const bool Initialize(ID3D12Device* Device, const ResMesh& Resource);

    /// <summary>
    /// 破棄
    /// </summary>
    void Termiante();

    /// <summary>
    /// 描画
    /// </summary>
    /// <param name="CmdList"></param>
    void Draw(ID3D12GraphicsCommandList* CmdList);

    /// <summary>
    /// マテリアルIDを返す
    /// </summary>
    /// <returns></returns>
    const uint32_t GetMaterialID() const;

private:
    // コピー禁止
    Mesh(const Mesh&) = delete;
    void operator=(const Mesh&) = delete;

    VertexBuffer VB = {};
    IndexBuffer IB = {};
    uint32_t MaterialID = 0;
    uint32_t IndexCount = 0;
};
=======
#include <d3d12.h>
#include <DirectXMath.h>
#include <string>
#include <vector>

// Mesh's Vertex
struct MeshVertex
{
    MeshVertex() = default;

    MeshVertex(const DirectX::XMFLOAT3& Position,
        const DirectX::XMFLOAT3& Normal,
        const DirectX::XMFLOAT2& TexCoord,
        const DirectX::XMFLOAT3& Tangent )
        : Position(Position)
        , Normal(Normal)
        , TexCoord(TexCoord)
        , Tangent(Tangent)
    {}

    
    static const D3D12_INPUT_LAYOUT_DESC InputLayout;

private:
    static const int InputElementCount = 4;
    static const D3D12_INPUT_ELEMENT_DESC InputElements[InputElementCount];
   
public:
    DirectX::XMFLOAT3 Position = {};
    DirectX::XMFLOAT3 Normal = {};
    DirectX::XMFLOAT2 TexCoord = {};
    DirectX::XMFLOAT3 Tangent = {};
};

// Material
struct Material
{
    DirectX::XMFLOAT3 Diffuse;
    DirectX::XMFLOAT3 Specular;
    float Alpah;
    float Shiness;
    std::string DiffuseMap;
};

// Mesh
struct Mesh
{
    std::vector<MeshVertex> Vertices;
    std::vector<uint32_t> Indices;
    uint32_t MaterialID;
};

const bool LoadMesh(const wchar_t* FileName, std::vector<Mesh>& Meshes, std::vector<Material>& Materials);
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
