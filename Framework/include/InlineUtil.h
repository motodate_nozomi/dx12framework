#pragma once

template<typename T>
__forceinline void SafeDelete(T*& Ptr)
{
    if (Ptr)
    {
        delete Ptr;
        Ptr = nullptr;
    }
}

template<typename T>
__forceinline void SafeDeleteArray(T*& Ptr)
{
    if (Ptr)
    {
        delete[] Ptr;
        Ptr = nullptr;
    }
}

template<typename T>
__forceinline void SafeRelease(T*& Ptr)
{
    if (Ptr)
    {
        Ptr->Release();
        Ptr = nullptr;
    }
}

template<typename T>
__forceinline void SafeTerminate(T*& Ptr)
{
    if (Ptr)
    {
        Ptr->Terminate();
        Ptr = nullptr;
    }
}