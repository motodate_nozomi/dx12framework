#pragma once

#include <wrl/client.h>

// 型エイリアス
template<typename T>
using ComPtr = Microsoft::WRL::ComPtr<T>;