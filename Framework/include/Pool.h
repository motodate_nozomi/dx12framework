#pragma once

#include <cstdint>
#include <mutex>
#include <cassert>
#include <functional>
#include <list>

// 汎用プール
template<typename T>
class Pool
{
public:

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="ItemCount"></param>
    /// <returns></returns>
    const bool Initialize(const uint32_t ItemCount)
    {
        std::lock_guard<std::mutex> Guard(Mutex);

        Capacity = ItemCount;
        FreeItemList.resize(ItemCount);

        int ItemCounter = 0;
        for (Item* TargetItem : FreeItemList)
        {
            TargetItem = new Item();
            if (TargetItem)
            {
                TargetItem->Index = ItemCounter;
                ++ItemCounter;
            }
        }

        return true;
    }

    /// <summary>
    /// 破棄
    /// </summary>
    void Terminate()
    {
        // アクティブリストの削除
        auto Itr = ActiveItemList.begin();
        while (Itr != ActiveItemList.end())
        {
            delete *Itr;
            *Itr = nullptr;
            ++Itr;
        }
        ActiveItemList.clear();

        // フリーリストの削除
        Itr = FreeItemList.begin();
        while (Itr != FreeItemList.end())
        {
            delete* Itr;
            *Itr = nullptr;
            ++Itr;
        }
        FreeItemList.clear();
    }

    /// <summary>
    /// アイテム確保
    /// </summary>
    /// <param name="InitializeFunc"></param>
    /// <returns></returns>
    T* const Alloc(std::function<void(uint32_t, T*)> InitializeFunc = nullptr)
    {
        std::lock_guard<std::mutex> guard(Mutex);

        if (FreeItemList.size() <= 0)
        {
            return nullptr;
        }

        Item* TargetItem = *FreeItemList.begin();
        if (!TargetItem)
        {
            return nullptr;
        }
        ++Count;

        // 確保
        T* NewValue = new ((void*)TargetItem) T();

        // 初期化関数でValueを変更
        if (InitializeFunc)
        {
            InitializeFunc(TargetItem->Index, NewValue);
        }

        // アクティブリストに追加
        ActiveItemList.emplace_back(TargetItem);

        return NewValue;
    }

    /// <summary>
    /// 要素の解放
    /// </summary>
    /// <param name="Value"></param>
    void Free(T* const Value)
    {
        std::lock_guard<std::mutex> guard(Mutex);

        if (!Value)
        {
            return;
        }

        Item* TargetItem = reinterpret_cast<Item*>(Value);
        ActiveItemList.remove(TargetItem);
        FreeItemList.emplace_back(TargetItem);
        --Count;
    }

    /// <summary>
    /// 最大アイテム数を返す
    /// </summary>
    /// <returns></returns>
    const uint32_t GetCapacity() const
    {
        return Capacity;
    }

    /// <summary>
    /// 使用中のアイテム数
    /// </summary>
    /// <returns></returns>
    const uint32_t GetUsedCount() const
    {
        return Count;
    }

    /// <summary>
    /// 利用可能アイテム数
    /// </summary>
    const uint32_t GetAvailableCount() const
    {
        return Capacity - Count;
    }

private:
    // アイテム
    struct Item
    {
        // 実際に使用する値、Allocの引数で渡す初期化関数で変更できる
        T Value = {};
        uint32_t Index = 0;
    };

    std::list<Item*> ActiveItemList = {};
    std::list<Item*> FreeItemList = {};



    uint8_t* Buffer = nullptr;
    Item* ActiveListHead = nullptr;
    Item* FreeListHead = nullptr;
    uint32_t Capacity = 0;
    uint32_t Count = 0;
    std::mutex Mutex = {};

    /// <summary>
    /// アイテムを返す
    /// </summary>
    /// <param name="Index"></param>
    /// <returns></returns>
    Item* const GetItem(const uint32_t Index)
    {
        if (Index < 0 || Index > Capacity + 1)
        {
            return nullptr;
        }
        return reinterpret_cast<Item*>(Buffer + sizeof(Item) * Index);
    }

    /// <summary>
    /// アイテムをメモリに割り当てる
    /// </summary>
    /// <param name="Index"></param>
    /// <returns></returns>
    Item* const AssignItem(const uint32_t Index)
    {
        auto BufferHandle = Buffer + sizeof(Item) * Index;
        return new (BufferHandle) Item;
    }
};
