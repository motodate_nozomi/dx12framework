#pragma once

#include <Windows.h>
#include <cstdint>
#include <d3d12.h>
#include <dxgi1_4.h>
#include <wrl/client.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>
#include "ResMesh.h"

#include "Mesh.h"

#pragma comment( lib, "d3d12.lib" )  
#pragma comment( lib, "dxgi.lib" )   
#pragma comment( lib, "d3dcompiler.lib" )
#pragma comment( lib, "dxguid.lib" )

template<typename T> using ComPtr = Microsoft::WRL::ComPtr<T>;

<<<<<<< HEAD
// 姿勢行列
=======
// Transform
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
struct alignas(256) Transform
{
    DirectX::XMMATRIX World;
    DirectX::XMMATRIX View;
    DirectX::XMMATRIX Proj;
};

<<<<<<< HEAD
// 定数バッファ
=======
// ConstantBuffer
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
template<typename T>
struct ConstantBufferView
{
    D3D12_CONSTANT_BUFFER_VIEW_DESC Desc;
    D3D12_CPU_DESCRIPTOR_HANDLE HandleCPU;
    D3D12_GPU_DESCRIPTOR_HANDLE HandleGPU;
    T* Buffer;
};

<<<<<<< HEAD
// テクスチャ
struct TextureRes
{
    ComPtr<ID3D12Resource> pResource = nullptr;
    // ディスクリプタのハンドル
    D3D12_CPU_DESCRIPTOR_HANDLE HandleCPU = {};
    D3D12_GPU_DESCRIPTOR_HANDLE HandleGPU = {};
=======
// Texture
struct Texture
{
    ComPtr<ID3D12Resource> pResource;
    D3D12_CPU_DESCRIPTOR_HANDLE HandleCPU;
    D3D12_GPU_DESCRIPTOR_HANDLE HandleGPU;
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
};

class App
{
public:
    App(const uint32_t Width, const uint32_t Height);
    ~App();

    void Run();

private:  
    const bool InitializeApp();
    void TerminateApp();
    const bool InitializeWindow();
    void TerminateWindow();
    void MainLoop();
    bool InitializeD3D();
    void TerminateD3D();
    void Render();
    void WaitGPU();
    void Present(uint32_t Interval);

    const bool OnInitialize();
    void OnTerminate();

    static LRESULT CALLBACK WndProc(HWND HWnd, UINT Msg, WPARAM Wp, LPARAM Lp);

    // regard window parameter
    HINSTANCE HInstance = nullptr;
    HWND HWnd = nullptr;
    uint32_t Width = 960;
    uint32_t Height = 540;

    // d3d12
    static const uint32_t FrameCount = 2;
    ComPtr<ID3D12Device> D3D12Device = nullptr;
    ComPtr<ID3D12CommandQueue> CommandQueue = nullptr;
    ComPtr<IDXGISwapChain3> SwapChain = nullptr;
    ComPtr<ID3D12Resource> ColorBuffer[FrameCount] = {};
    ComPtr<ID3D12CommandAllocator> CommandAllocator[FrameCount] = {};
    ComPtr<ID3D12GraphicsCommandList> CommandList = nullptr;
    ComPtr<ID3D12DescriptorHeap> HeapRTV = nullptr;
    ComPtr<ID3D12Fence> Fence = nullptr;
    HANDLE FenceEvent = nullptr;
    uint64_t FenceCounter[FrameCount] = {};
    uint32_t FrameIndex = 0;
    D3D12_CPU_DESCRIPTOR_HANDLE HandleRTV[FrameCount] = {};

    // for polygon
    ComPtr<ID3D12DescriptorHeap> HeapCBV_SRV_UAV = nullptr;
    ComPtr<ID3D12Resource> VertexBuffer = nullptr;
    ComPtr<ID3D12Resource> IndexBuffer = nullptr;
    ComPtr<ID3D12Resource> ConstantBuffer[FrameCount] = {};
    ComPtr<ID3D12RootSignature> RootSignature = nullptr;
    ComPtr<ID3D12PipelineState> PSO = nullptr;
    D3D12_VERTEX_BUFFER_VIEW VBView = {};
    D3D12_INDEX_BUFFER_VIEW IBView = {};
    D3D12_VIEWPORT Viewport = {};
    D3D12_RECT Scissor = {};
    ConstantBufferView<Transform> CBView[FrameCount * 2] = {};
    float RotateAngle = 0.0f;

    // DepthStencilBuffer
    ComPtr<ID3D12Resource> DepthStencilBuffer = nullptr;
    ComPtr<ID3D12DescriptorHeap> HeapDSV = nullptr;
    D3D12_CPU_DESCRIPTOR_HANDLE HandleDSV = {}; // View for DepthStencil

<<<<<<< HEAD
    TextureRes ColorTexture = {};

    // MeshData
    std::vector<ResMesh> StaticMeshes = {};
    std::vector<ResMaterial> StaticMeshMaterials = {};
=======
    // Texture
    Texture Texture_;

    // Mesh
    std::vector<Mesh> Meshes = {};
    std::vector<Material> Materials = {};
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
};

