#pragma once

#include <d3d12.h>
#include <ComPtr.h>
#include <ResourceUploadBatch.h>

class DescriptorHandle;
class DescriptorPool;

class Texture
{
public:
    Texture();

    ~Texture();

    const bool Initialize(
        ID3D12Device* Device,
        DescriptorPool* Pool,
        const wchar_t* Filename,
        DirectX::ResourceUploadBatch& Batch);

    const bool Initialize(
        ID3D12Device* Device,
        DescriptorPool* Pool,
        const D3D12_RESOURCE_DESC* Desc,
        const bool bIsCube);

    void Terminate();

    const D3D12_CPU_DESCRIPTOR_HANDLE GetCPUHandle() const;

    const D3D12_GPU_DESCRIPTOR_HANDLE GetGPUHandle() const;

private:
    // �R�s�[�֎~
    Texture(const Texture&) = delete;
    void operator=(const Texture&) = delete;

    D3D12_SHADER_RESOURCE_VIEW_DESC GetViewDesc(const bool bIsCube);

    ComPtr<ID3D12Resource> Resource = nullptr;
    DescriptorHandle* Handle = nullptr;
    DescriptorPool* Pool = nullptr;
};