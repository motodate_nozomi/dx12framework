#pragma once

#include <d3d12.h>
#include <ComPtr.h>

class VertexBuffer
{
public:
    VertexBuffer();
    ~VertexBuffer();

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="Device">デバイス</param>
    /// <param name="BufferSize">バッファのサイズ</param>
    /// <param name="Stride">区切り</param>
    /// <param name="InitData">初期データ</param>
    /// <returns></returns>
    const bool Initialize(
        ID3D12Device* Device,
        size_t BufferSize,
        size_t Stride,
        const void* InitData = nullptr);

    template<typename T>
    const bool Initialize(
        ID3D12Device* Device,
        size_t BufferSize,
        const void* InitData = nullptr)
    {
        return Initialize(Device, BufferSize, sizeof(T), InitData);
    }

    /// <summary>
    /// 破棄
    /// </summary>
    void Terminate();

    void* Map();

    template<typename T>
    T* const Map() const {
        reinterpret_cast<T*>(Map());
    }

    void Unmap();

    /// <summary>
    /// 頂点バッファービューを返す
    /// </summary>
    /// <returns></returns>
    const D3D12_VERTEX_BUFFER_VIEW GetView() const;

private:
    // コピー禁止
    VertexBuffer(const VertexBuffer&) = delete;
    void operator=(const VertexBuffer&) = delete;

    ComPtr<ID3D12Resource> Resource = nullptr;
    D3D12_VERTEX_BUFFER_VIEW View = {};
};