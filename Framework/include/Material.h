#pragma once

#include <DescriptorPool.h>
#include <ResourceUploadBatch.h>
#include <Texture.h>
#include <ConstantBuffer.h>
#include <map>
#include <vector>

class Material
{
public:
    enum TEXTURE_USAGE
    {
        TEXTURE_USAGE_DIFFUSE = 0,
        TEXTURE_USAGE_SPECULAR,
        TEXTURE_USAGE_SHININESS,
        TEXTURE_USAGE_NORMAL,

        TEXTURE_USAGE_COUNT,
    };

    Material();
    ~Material();

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="Device">D3D12デバイス</param>
    /// <param name="Pool">ディスクリプタプール</param>
    /// <param name="CBufferSize">定数バッファサイズ</param>
    /// <param name="MaterialCount">マテリアル数</param>
    /// <returns></returns>
    const bool Initialize(
        ID3D12Device* Device,
        DescriptorPool* Pool,
        size_t CBufferSize,
        size_t MaterialCount);

    /// <summary>
    /// 破棄
    /// </summary>
    void Termiante();

    /// <summary>
    /// テクスチャを設定する
    /// </summary>
    /// <param name="Index"></param>
    /// <param name="Usage"></param>
    /// <param name="Path"></param>
    /// <param name="Batch"></param>
    /// <returns></returns>
    const bool SetTexture(
        size_t MaterialIndex,
        TEXTURE_USAGE Usage,
        const std::wstring& Path,
        DirectX::ResourceUploadBatch& Batch);

   /// <summary>
   /// 定数バッファのGPU仮想アドレス
   /// </summary>
   /// <param name="Index"></param>
   /// <returns></returns>
   const D3D12_GPU_VIRTUAL_ADDRESS GetCBufferVirtualAddress(const size_t MaterialIndex) const;

   /// <summary>
   /// テクスチャのハンドル
   /// </summary>
   /// <param name="Index"></param>
   /// <param name="Usage"></param>
   /// <returns></returns>
   const D3D12_GPU_DESCRIPTOR_HANDLE GetTextureGPUHandle(size_t MaterialIndex, TEXTURE_USAGE Usage) const ;

    /// <summary>
    /// 定数バッファのアドレスを返す
    /// </summary>
    /// <param name="Index"></param>
    /// <returns></returns>
    void* const GetCBufferPtr(const size_t Index) const;

    template<typename T>
    T* const GetCBufferPtr(const size_t Index) const {
        return reinterpret_cast<T*>(GetCBufferPtr(Index));
    }

    /// <summary>
    /// マテリアル数
    /// </summary>
    /// <returns></returns>
    const size_t GetMaterialCount() const;

private:
    // コピー禁止
    Material(const Material&) = delete;
    void operator=(const Material&) = delete;

    class Subset
    {
    public:
        ConstantBuffer* CBuffer = nullptr;
        D3D12_GPU_DESCRIPTOR_HANDLE TextureHandle[TEXTURE_USAGE_COUNT];
    };

    std::map<std::wstring, Texture*> TextureMap = {};
    std::vector<Subset> SubsetList = {};
    ID3D12Device* Device = nullptr;
    DescriptorPool* Pool = nullptr;
};

constexpr auto TU_DIFFUSE = Material::TEXTURE_USAGE_DIFFUSE;
constexpr auto TU_SPECULAR = Material::TEXTURE_USAGE_SPECULAR;
constexpr auto TU_SHININESS = Material::TEXTURE_USAGE_SHININESS;
constexpr auto TU_NORMAL = Material::TEXTURE_USAGE_NORMAL;