#pragma once

#include <d3d12.h>
#include <ComPtr.h>
#include <cstdint>
#include <vector>

class CommandList
{
public:
    CommandList();
    ~CommandList();

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="Device">デバイス</param>
    /// <param name="CmdType">コマンドリストの種類</param>
    /// <param name="AllocatorNum">ダブルバッファするなら2を設定</param>
    /// <returns></returns>
    const bool Initialize(ID3D12Device* const Device, 
        D3D12_COMMAND_LIST_TYPE CmdType, uint32_t AllocatorNum = 2);

    /// <summary>
    /// 破棄
    /// </summary>
    void Terminate();

    /// <summary>
    /// コマンドリストをリセット
    /// </summary>
    /// <returns></returns>
    ID3D12GraphicsCommandList* const Reset();

private:
    // コピー禁止
    CommandList(const CommandList&) = delete;
    void operator=(const CommandList&) = delete;

    ComPtr<ID3D12GraphicsCommandList> CmdList = nullptr;
    std::vector<ComPtr<ID3D12CommandAllocator>> CmdAllocatorList = {};
    uint32_t AllocatorIndex = 0;
};