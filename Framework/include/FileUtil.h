#pragma once

#include <string>
#include <Shlwapi.h>

#pragma comment(lib, "shlwapi.lib")

<<<<<<< HEAD
/// <summary>
/// ファイルパス検索
/// </summary>
const bool SearchFilePathA(const char* filename, std::string& result);

/// <summary>
/// ファイルパス検索
/// </summary>
const bool SearchFilePathW(const wchar_t* filename, std::wstring& result);

/// <summary>
/// ファイル名を返す
/// </summary>
std::string GetFileNameA(const std::string& Path);

/// <summary>
/// ファイル名を返す
/// </summary>
std::wstring GetFileNameW(const std::wstring& Path);

/// <summary>
/// ディレクトリ名
/// </summary>
std::string GetDirectoryNameA(const char* path);

/// <summary>
/// ディレクトリ名
/// </summary>
std::wstring GetDirectoryNameW(const wchar_t* path);

#if defined(UNICODE) || defined(_UNICODE)
__forceinline const bool SearchFilePath(const wchar_t* filename, std::wstring& result)
{
    return SearchFilePathW(filename, result);
}

__forceinline std::wstring GetFileName(const std::wstring& Path)
{
    return GetFileNameW(Path);
}

__forceinline std::wstring GetDirectoryName(const wchar_t* Path)
{
    return GetDirectoryNameW(Path);
}

#else
__forceinline const bool SearchFilePath(const char* filename, std::string& result)
{
    return SearchFilePathA(filename, result);
}

__forceinline std::string GetFileName(const std::string& Path)
{
    return GetFileNameA(Path);
}

__forceinline std::string GetDirectoryName(const char* Path)
{
    return GetDirectoryNameA(Path);
}

#endif // defined(UNICODE) || defined(_UNICODE)
=======
// Check exist file
const bool SearchFilePahtA(const char* FileName, std::string& Result);

const bool SearchFilePathW(const wchar_t* FileName, std::wstring& Result);

// branch use charSet
#if defined(UNICODE) || defined(_UNICODE)
__forceinline const bool SearchFilePath(const wchar_t* FileName, std::wstring& Result)
{
    return SearchFilePathW(FileName, Result);
}
#else
__forceinline const bool SearchFilePath(const wchar_t* FileName, std::wstring& Result)
{
    return SearchFilePahtA(FileName, Result);
}
#endif // defined(UNICODE) || defined(_UNICODE)
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
