﻿#pragma once

#include <Windows.h>
#include <cstdint>
#include <d3d12.h>
#include <dxgi1_4.h>
#include <d3dcompiler.h>
#include <ComPtr.h>
#include <DescriptorPool.h>
#include <ColorTarget.h>
#include <DepthTarget.h>
#include <CommandList.h>
#include <Fence.h>
#include <Mesh.h>
#include <Texture.h>
#include <InlineUtil.h>

#pragma  comment(lib, "d3d12.lib")
#pragma  comment(lib, "dxgi.lib")
#pragma  comment(lib, "dxguid.lib")
#pragma  comment(lib, "d3dcompiler.lib")

class BaseApp
{
public:
    BaseApp(const uint32_t Width, const uint32_t Height);
    virtual ~BaseApp();

    void Run();

protected:
    void Present(const uint32_t Interval);

    virtual const bool OnInitialize()
    {
        return true;
    }

    virtual void OnTerminate()
    {}

    virtual void OnRender()
    {}

    virtual void OnMsgProc(HWND, UINT, WPARAM, LPARAM)
    {}

    enum POOL_TYPE
    {
        POOL_TYPE_RES = 0, // CBV, SRV, UAV
        POOL_TYPE_SMP = 1, // Sampler
        POOL_TYPE_RTV = 2, // RTV
        POOL_TYPE_DSV = 3, // DSV
        POOL_COUNT = 4,
    };

    // フレームバッファ数
    static const uint32_t FrameCount = 2;

    HINSTANCE HInstance = {};
    HWND HWnd = {};
    uint32_t Width = 0;
    uint32_t Height = 0;

    ComPtr<ID3D12Device> D3D12Device = nullptr;
    ComPtr<ID3D12CommandQueue> GFXCommandQueue = nullptr;
    ComPtr<IDXGISwapChain3> SwapChain = nullptr;
    ColorTarget ColorTargetList[FrameCount];
    DepthTarget DepthStencilTarget;
    DescriptorPool* Pool[POOL_COUNT];
    CommandList GFXCommandList;
    Fence GPUFence = {};
    uint32_t FrameIndex = {};
    D3D12_VIEWPORT Viewport = {};
    D3D12_RECT Scissor = {};

private:

    // アプリ
    const bool InitializeApp();
    void TermianteApp();

    // ウィンドウ
    const bool InitializeWindow();
    void TermianteWindow();

    // D3D
    const bool InitializeD3D();
    void TerminateD3D();

    // ループ
    void MainLoop();

    // ウィンドプロシージャ
    static LRESULT CALLBACK WndProc(HWND HWnd, UINT Msg, WPARAM Wp, LPARAM Lp);
};