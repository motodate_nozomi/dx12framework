#pragma once

#include <d3D12.h>
#include <DirectXMath.h>
#include <string>
#include <vector>

// メッシュの頂点データ
class MeshVertex
{
public:
    MeshVertex() = default;

    MeshVertex(const DirectX::XMFLOAT3& aPosition, const DirectX::XMFLOAT3& aNormal,
        const DirectX::XMFLOAT2& aTexCoord, const DirectX::XMFLOAT3& aTangent)
    : Position(aPosition)
    , Normal(aNormal)
    , TexCoord(aTexCoord)
    , Tangent(aTangent) 
    { /* Do nothing. */ }

    DirectX::XMFLOAT3 Position = {};
    DirectX::XMFLOAT3 Normal = {};
    DirectX::XMFLOAT2 TexCoord = {};
    DirectX::XMFLOAT3 Tangent = {};

    static const D3D12_INPUT_LAYOUT_DESC InputLayout;

private:
    static const int InputElementCount = 4;
    static const D3D12_INPUT_ELEMENT_DESC InputElements[InputElementCount];

};

// マテリアル
class ResMaterial
{
public:
    // 拡散反射
    DirectX::XMFLOAT3 Diffuse = {};
    // 鏡面反射
    DirectX::XMFLOAT3 Specular = {};
    // 透過度
    float Alpha = 0.0f;
    // 鏡面反射強度
    float Shininess = 0.0f;
    // テクスチャパス
    std::string DiffuseMap = {};
};

class ResMesh
{
public:
    // 頂点データ
    std::vector<MeshVertex> Vertices = {};
    // インデックスデータ
    std::vector<uint32_t> Indices = {};
    // マテリアルID
    uint32_t MaterialID;
};

/// <summary>
/// メッシュの読み込み
/// </summary>
const bool LoadMesh(const wchar_t* MeshFilename, std::vector<ResMesh>& OutMeshes, std::vector<ResMaterial>& OutMaterials);