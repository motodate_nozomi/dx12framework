#pragma once

#include <d3d12.h>
#include <ComPtr.h>
#include <cstdint>

class DescriptorHandle;
class DescriptorPool;

class DepthTarget
{
public:
    DepthTarget();
    ~DepthTarget();

    const bool Initialize(
        ID3D12Device* const Device,
        DescriptorPool* const DSVPool,
        uint32_t Width,
        uint32_t Height,
        DXGI_FORMAT Format);

    void Termiante();

    DescriptorHandle* const GetDSVHandle() const;

    ID3D12Resource* const GetResource() const;

    const D3D12_RESOURCE_DESC GetDesc() const;

    const D3D12_DEPTH_STENCIL_VIEW_DESC GetViewDesc() const;

private:
    // �R�s�[�֎~
    DepthTarget(const DepthTarget&) = delete;
    void operator=(const DepthTarget&) = delete;

    ComPtr<ID3D12Resource> Resource = nullptr;
    DescriptorHandle* DSVHandle = nullptr;
    DescriptorPool* DSVPool = nullptr;
    D3D12_DEPTH_STENCIL_VIEW_DESC ViewDesc = {};
};