#pragma once

#include <d3d12.h>
#include <ComPtr.h>
#include <vector>

class DescriptorHandle;
class DescriptorPool;

class ConstantBuffer
{
public:
    ConstantBuffer();
    ~ConstantBuffer();

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="Device"></param>
    /// <param name="Pool"></param>
    /// <param name="Size"></param>
    /// <returns></returns>
    const bool Initialize(
        ID3D12Device* Device,
        DescriptorPool* Pool,
        size_t Size);

    /// <summary>
    /// 破棄
    /// </summary>
    void Terminate();

    /// <summary>
    /// GPU仮想アドレスを返す
    /// </summary>
    /// <returns></returns>
    const D3D12_GPU_VIRTUAL_ADDRESS GetVirtualAdress() const;

    /// <summary>
    /// CPUハンドルを返す
    /// </summary>
    /// <returns></returns>
    const D3D12_CPU_DESCRIPTOR_HANDLE GetCPUHandle() const;

    /// <summary>
    /// GPUハンドルを返す
    /// </summary>
    /// <returns></returns>
    const D3D12_GPU_DESCRIPTOR_HANDLE GetGPUHandle() const;

    /// <summary>
    /// メモリマッピング済ポインタを返す
    /// </summary>
    /// <returns></returns>
    void* const GetMappedPtr() const;

    template<typename T>
    T* const GetMappedPtr() const
    {
        return reinterpret_cast<T*>(GetMappedPtr());
    }

private:
    // コピー禁止
    ConstantBuffer(const ConstantBuffer&) = delete;
    void operator=(const ConstantBuffer&) = delete;

    ComPtr<ID3D12Resource> Resource = nullptr;
    DescriptorHandle* Handle = nullptr;
    DescriptorPool* Pool = nullptr;
    D3D12_CONSTANT_BUFFER_VIEW_DESC ViewDesc = {};
    void* MappedPtr = nullptr;
};