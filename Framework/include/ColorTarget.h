#pragma once

#include <d3d12.h>
#include <dxgi1_6.h>
#include <ComPtr.h>
#include <cstdint>

class DescriptorHandle;
class DescriptorPool;

class ColorTarget
{
public:
    ColorTarget();
    ~ColorTarget();

    const bool Initialize(
        ID3D12Device* Device,
        DescriptorPool* Pool,
        uint32_t Width,
        uint32_t Height,
        DXGI_FORMAT Format);

    const bool InitializeFromBackBuffer(
        ID3D12Device* const Device,
        DescriptorPool* const Pool,
        uint32_t BackBufferIndex,
        IDXGISwapChain* const SwapChain);

    void Terminate();

    DescriptorHandle* const GetRTVHandle() const;

    ID3D12Resource* const GetResource() const;

    const D3D12_RESOURCE_DESC GetResourceDesc() const;

    const D3D12_RENDER_TARGET_VIEW_DESC GetRTVDesc() const;

private:
    // �R�s�[�֎~
    ColorTarget(const ColorTarget&) = delete;
    void operator=(const ColorTarget&) = delete;

    ComPtr<ID3D12Resource> Resource = nullptr;
    DescriptorHandle* RTVHandle = nullptr;
    DescriptorPool* RTVPool = nullptr;
    D3D12_RENDER_TARGET_VIEW_DESC ViewDesc = {};
};