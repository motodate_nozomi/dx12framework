#include "Material.h"
#include "FileUtil.h"
#include "Logger.h"

namespace {
    const wchar_t* DummyTag = L"";
}

Material::Material()
{

}

Material::~Material()
{
    Termiante();
}

const bool Material::Initialize(
    ID3D12Device* Device,
    DescriptorPool* Pool,
    size_t CBufferSize,
    size_t MaterialCount)
{
    if (!Device)
    {
        ELOG("Material::Initialize >> Device が nullptr.");
        return false;
    }

    if (!Pool)
    {
        ELOG("Material::Initialize >> Pool が nullptr.");
        return false;
    }

    if (MaterialCount <= 0)
    {
        ELOG("Material::Initialize >> MaterialCount <= 0.");
        return false;
    }

    Termiante();

    this->Device = Device;
    Device->AddRef();

    this->Pool = Pool;
    Pool->AddRef();

    SubsetList.resize(MaterialCount);

    // ダミーテクスチャ生成
    {
        auto DummyTexture = new(std::nothrow) Texture();
        if (!DummyTexture)
        {
            ELOG("Material::Initialize >> ダミーテクスチャの生成に失敗.");
            return false;
        }

        D3D12_RESOURCE_DESC Desc = {};
        Desc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
        Desc.Width = 1;
        Desc.Height = 1;
        Desc.DepthOrArraySize = 1;
        Desc.MipLevels = 1;
        Desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
        Desc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
        Desc.SampleDesc.Count = 1;
        Desc.SampleDesc.Quality = 0;

        if (!DummyTexture->Initialize(Device, Pool, &Desc, false))
        {
            ELOG("Material::Initialize >> ダミーテクスチャの生成に失敗.");
            DummyTexture->Terminate();
            delete DummyTexture;
            return false;
        }

        TextureMap[DummyTag] = DummyTexture;
    }

    auto Size = CBufferSize * MaterialCount;
    if (Size > 0)
    {
        for (size_t SubsetIndex = 0; SubsetIndex < SubsetList.size(); ++SubsetIndex)
        {
            auto Buffer = new(std::nothrow) ConstantBuffer();
            if (!Buffer)
            {
                ELOG("Material::Initialize >> バッファの生成に失敗.");
                return false;
            }

            if (!Buffer->Initialize(Device, Pool, CBufferSize))
            {
                ELOG("Material::Initialize >> バッファの初期化に失敗.");
                return false;
            }

            SubsetList[SubsetIndex].CBuffer = Buffer;
            for (auto UsageCount = 0; UsageCount < TEXTURE_USAGE_COUNT; ++UsageCount)
            {
                SubsetList[SubsetIndex].TextureHandle[UsageCount].ptr = 0;
            }
        }
    }
    else
    {
        for (size_t Index = 0; Index < SubsetList.size(); ++Index)
        {
            SubsetList[Index].CBuffer = nullptr;
            for (auto UsageIndex = 0; UsageIndex < TEXTURE_USAGE_COUNT; ++UsageIndex)
            {
                SubsetList[Index].TextureHandle[UsageIndex].ptr = 0;
            }
        }
    }

    return true;
}

void Material::Termiante()
{
    // テクスチャの削除
    for (auto& Itr : TextureMap)
    {
        if (!Itr.second)
        {
            Itr.second->Terminate();
            delete Itr.second;
            Itr.second = nullptr;
        }
    }

    for (size_t SubIndex = 0; SubIndex < SubsetList.size(); ++SubIndex)
    {
        if (!SubsetList[SubIndex].CBuffer)
        {
            SubsetList[SubIndex].CBuffer->Terminate();
            delete SubsetList[SubIndex].CBuffer;
            SubsetList[SubIndex].CBuffer = nullptr;
        }
    }

    TextureMap.clear();
    SubsetList.clear();

    if (Device)
    {
        Device->Release();
        Device = nullptr;
    }

    if (Pool)
    {
        Pool->Release();
        Pool = nullptr;
    }
}

const bool Material::SetTexture(
    size_t MaterialIndex, 
    TEXTURE_USAGE TextureUsage,
    const std::wstring& TexturePath,
    DirectX::ResourceUploadBatch& Batch)
{
    if (MaterialIndex >= GetMaterialCount())
    {
        ELOG("Material::SetTexture >> マテリアルが存在しません.");
        return false;
    }

    // 登録済ならそれを使用
    if (TextureMap.find(TexturePath) != TextureMap.end())
    {
        SubsetList[MaterialIndex].TextureHandle[TextureUsage] = 
            TextureMap[TexturePath]->GetGPUHandle();
        return true;
    }

    // テクスチャのパスを検証
    std::wstring FindPath = {};
    if (!SearchFilePathW(TexturePath.c_str(), FindPath))
    {
        DLOG("Material::SetTexture >> ダミーテクスチャを使用(%s).", TexturePath.c_str());
        return true;
    }

    // 新しいテクスチャを生成
    auto NewTexture = new(std::nothrow) Texture();
    if (!NewTexture)
    {
        ELOG("Material::SetTexture >> 新しいテクスチャを作れませんでした.");
        return false;
    }

    if (!NewTexture->Initialize(Device, Pool, FindPath.c_str(), Batch))
    {
        ELOG("Material::SetTexture >> 新しいテクスチャの初期化に失敗.");
        NewTexture->Terminate();
        delete NewTexture;
        return false;
    }

    TextureMap[TexturePath] = NewTexture;
    SubsetList[MaterialIndex].TextureHandle[TextureUsage] = NewTexture->GetGPUHandle();

    return true;
}

const D3D12_GPU_VIRTUAL_ADDRESS Material::GetCBufferVirtualAddress(const size_t MaterialIndex) const
{
    if (MaterialIndex >= GetMaterialCount())
    {
        return D3D12_GPU_VIRTUAL_ADDRESS();
    }

    if (!SubsetList[MaterialIndex].CBuffer)
    {
        return D3D12_GPU_VIRTUAL_ADDRESS();
    }

    return SubsetList[MaterialIndex].CBuffer->GetVirtualAdress();
}

const D3D12_GPU_DESCRIPTOR_HANDLE Material::GetTextureGPUHandle(size_t MaterialIndex, TEXTURE_USAGE Usage) const
{
    if (MaterialIndex >= GetMaterialCount())
    {
        return D3D12_GPU_DESCRIPTOR_HANDLE();
    }

    return SubsetList[MaterialIndex].TextureHandle[Usage];
}

void* const Material::GetCBufferPtr(const size_t MaterialIndex) const
{
    if (MaterialIndex >= GetMaterialCount())
    {
        return nullptr;
    }

    if (!SubsetList[MaterialIndex].CBuffer)
    {
        return nullptr;
    }

    return SubsetList[MaterialIndex].CBuffer->GetMappedPtr();
}

const size_t Material::GetMaterialCount() const
{
    return SubsetList.size();
}
