#include "Mesh.h"
<<<<<<< HEAD
#include <Logger.h>

Mesh::Mesh()
=======

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <codecvt>
#include <cassert>

namespace {

    // 2 UTF8
    std::string ToUTF8(const std::wstring& Value)
    {
        auto ValueLenght = WideCharToMultiByte(CP_UTF8, 0U,
            Value.data(), -1, nullptr, 0, nullptr, nullptr);

        auto Buffer = new char[ValueLenght];

        WideCharToMultiByte(CP_UTF8, 0U, Value.data(), -1, Buffer, 
            ValueLenght, nullptr, nullptr);
    
        std::string Result(Buffer);
        delete[] Buffer;

        return Result;
    }

// MeshLoader
class MeshLoader
{
public:
    MeshLoader();
    ~MeshLoader();

    const bool Load(const wchar_t* FileName, std::vector<Mesh>& Meshes, std::vector<Material>& Materials);

private:
    void ParseMesh(Mesh& DistMesh, const aiMesh* pSrcMesh);
    void ParseMaterial(Material& DistMaterial, const aiMaterial* pSrcMaterial);
};

MeshLoader::MeshLoader()
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
{

}

<<<<<<< HEAD
Mesh::~Mesh()
{
    Termiante();
}

const bool Mesh::Initialize(ID3D12Device* Device, const ResMesh& Resource)
{
    if (!Device)
    {
        ELOG("Mesh::Initialize >> Device が nullptr.");
        return false;
    }

    bool bSuccess = VB.Initialize<MeshVertex>(Device,
        sizeof(MeshVertex) * Resource.Vertices.size(), Resource.Vertices.data());
    if (!bSuccess)
    {
        ELOG("Mesh::Initialize >> 頂点バッファの初期化に失敗.");
        return false;
    }

    bSuccess = IB.Initialize(Device,
        sizeof(uint32_t) * Resource.Indices.size(), Resource.Indices.data());
    if (!bSuccess)
    {
        ELOG("Mesh::Initialize >> インデックスバッファの初期化に失敗.");
        return false;
    }

    MaterialID = Resource.MaterialID;
    IndexCount = uint32_t(Resource.Indices.size());
=======
MeshLoader::~MeshLoader()
{

}

const bool MeshLoader::Load(const wchar_t* FileName, std::vector<Mesh>& Meshes, std::vector<Material>& Materials)
{
    if (!FileName)
    {
        return false;
    }

    // Load file
    auto Path = ToUTF8(FileName);

    Assimp::Importer Importer = {};
    int Flag = 0;
    Flag |= aiProcess_Triangulate;
    Flag |= aiProcess_PreTransformVertices;
    Flag |= aiProcess_CalcTangentSpace;
    Flag |= aiProcess_GenSmoothNormals;
    Flag |= aiProcess_GenUVCoords;
    Flag |= aiProcess_RemoveRedundantMaterials;
    Flag |= aiProcess_OptimizeMeshes;

    auto pScene = Importer.ReadFile(Path, Flag);

    if (!pScene)
    {
        return false;
    }

    // Reserve memory for Meshes
    Meshes.clear();
    Meshes.resize(pScene->mNumMeshes);

    // Transform MeshData
    for (size_t MeshCount = 0; MeshCount < Meshes.size(); ++MeshCount)
    {
        const auto pMesh = pScene->mMeshes[MeshCount];
        ParseMesh(Meshes[MeshCount], pMesh);
    }

    // Reserve memory for Materials
    Materials.clear();
    Materials.resize(pScene->mNumMaterials);

    // Transform MaterialData
    for (size_t MaterialCount = 0; MaterialCount < Materials.size(); ++MaterialCount)
    {
        const auto pMaterial = pScene->mMaterials[MaterialCount];
        ParseMaterial(Materials[MaterialCount], pMaterial);
    }

    pScene = nullptr;
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55

    return true;
}

<<<<<<< HEAD
void Mesh::Termiante()
{
    VB.Terminate();
    IB.Terminate();
    MaterialID = 0;
    IndexCount = 0;
}

void Mesh::Draw(ID3D12GraphicsCommandList* CmdList)
{
    if (!CmdList)
    {
        ELOG("Mesh::Draw >> コマンドリストがnullptr.");
        return;
    }

    auto VBView = VB.GetView();
    auto IBView = IB.GetView();
    CmdList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
    CmdList->IASetVertexBuffers(0, 1, &VBView);
    CmdList->IASetIndexBuffer(&IBView);
    CmdList->DrawIndexedInstanced(IndexCount, 1, 0, 0, 0);
}

const uint32_t Mesh::GetMaterialID() const
{
    return MaterialID;
}
=======
void MeshLoader::ParseMesh(Mesh& DistMesh, const aiMesh* pSrcMesh)
{
    // Set MaterialNum
    DistMesh.MaterialID = pSrcMesh->mMaterialIndex;

    aiVector3D Zero3D(0.0f, 0.0f, 0.0f);

    // Reserve memory for VertexData
    DistMesh.Vertices.resize(pSrcMesh->mNumVertices);

    for (auto VertexCount = 0u; VertexCount < pSrcMesh->mNumVertices; ++VertexCount)
    {
        auto pPosition = &(pSrcMesh->mVertices[VertexCount]);
        auto pNormal = &(pSrcMesh->mNormals[VertexCount]);
        auto pTexCoord = (pSrcMesh->HasTextureCoords(0)) ? &(pSrcMesh->mTextureCoords[0][VertexCount]) : &Zero3D;
        auto pTangent = (pSrcMesh->HasTangentsAndBitangents()) ? &(pSrcMesh->mTangents[VertexCount]) : &Zero3D;
    
        DistMesh.Vertices[VertexCount] = MeshVertex(
            DirectX::XMFLOAT3(pPosition->x, pPosition->y, pPosition->z),
            DirectX::XMFLOAT3(pNormal->x, pNormal->y, pNormal->z),
            DirectX::XMFLOAT2(pTexCoord->x, pTexCoord->y),
            DirectX::XMFLOAT3(pTangent->x, pTangent->y, pTangent->z)
        );
    }

    // Reserve memory for Index
    DistMesh.Indices.resize(pSrcMesh->mNumFaces * 3);

    for (auto IndexCount = 0u; IndexCount < pSrcMesh->mNumFaces; ++IndexCount)
    {
        const auto& Face = pSrcMesh->mFaces[IndexCount];
        assert(Face.mNumIndices == 3);

        DistMesh.Indices[IndexCount * 3 + 0] = Face.mIndices[0];
        DistMesh.Indices[IndexCount * 3 + 1] = Face.mIndices[1];
        DistMesh.Indices[IndexCount * 3 + 2] = Face.mIndices[2];
    }
}

void MeshLoader::ParseMaterial(Material& DistMaterial, const aiMaterial* pSrcMaterial)
{
    if (!pSrcMaterial)
    {
        OutputDebugStringW(L"Material is nullptr.\n");
        return;
    }

    // Diffuse
    {
        aiColor3D Color(0.0f, 0.0f, 0.0f);

        if (pSrcMaterial->Get(AI_MATKEY_COLOR_DIFFUSE, Color) == AI_SUCCESS)
        {
            DistMaterial.Diffuse.x = Color.r;
            DistMaterial.Diffuse.y = Color.g;
            DistMaterial.Diffuse.z = Color.b;
        }
        else
        {
            DistMaterial.Diffuse.x = 0.5f;
            DistMaterial.Diffuse.y = 0.5f;
            DistMaterial.Diffuse.z = 0.5f;
        }
    }

    // Specular
    {
        aiColor3D Specular(0.0f, 0.0f, 0.0f);
        if (pSrcMaterial->Get(AI_MATKEY_COLOR_SPECULAR, Specular) == AI_SUCCESS)
        {
            DistMaterial.Specular.x = Specular.r;
            DistMaterial.Specular.y = Specular.g;
            DistMaterial.Specular.z = Specular.b;
        }
        else
        {
            DistMaterial.Specular.x = 0.0f;
            DistMaterial.Specular.y = 0.0f;
            DistMaterial.Specular.z = 0.0f;
        }
    }

    // Shiness
    {
        auto Shiness = 0.0f;
        if (pSrcMaterial->Get(AI_MATKEY_SHININESS, Shiness) == AI_SUCCESS)
        {
            DistMaterial.Shiness = Shiness;
        }
        else
        {
            DistMaterial.Shiness = 0.0f;
        }
    }

    // DiffuseMap
    {
        aiString Path = {};
        if (pSrcMaterial->Get(AI_MATKEY_TEXBLEND_DIFFUSE(0), Path) == AI_SUCCESS)
        {
            DistMaterial.DiffuseMap = std::string(Path.C_Str());
        }
        else
        {
            DistMaterial.DiffuseMap.clear();
        }
    }
}

} // end of namespace

// Constant Values
#define FMT_FLOAT3 DXGI_FORMAT_R32G32B32_FLOAT
#define FMT_FLOAT2 DXGI_FORMAT_R32G32_FLOAT
#define APPEND D3D12_APPEND_ALIGNED_ELEMENT
#define IL_VERTEX D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA

const D3D12_INPUT_ELEMENT_DESC MeshVertex::InputElements[] = {
    {"POSITION", 0, FMT_FLOAT3, 0, APPEND, IL_VERTEX, 0},
    {"NORMAL", 0, FMT_FLOAT3, 0, APPEND, IL_VERTEX, 0},
    {"TEXCOORD", 0, FMT_FLOAT2, 0, APPEND, IL_VERTEX, 0},
    {"TANGENT", 0, FMT_FLOAT3, 0, APPEND, IL_VERTEX, 0}
};

const D3D12_INPUT_LAYOUT_DESC MeshVertex::InputLayout = {
    MeshVertex::InputElements,
    MeshVertex::InputElementCount,
};

static_assert(sizeof(MeshVertex) == 44, "Vertex struct layout mismatch\n");

#undef FMT_FLOAT3
#undef FMT_FLOAT2
#undef APPEND
#undef IL_VERTEX

// Load Mesh
const bool LoadMesh(
    const wchar_t* FileName,
    std::vector<Mesh>& Meshes,
    std::vector<Material>& Materials
)
{
    MeshLoader Loader = {};
    return Loader.Load(FileName, Meshes, Materials);
}
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
