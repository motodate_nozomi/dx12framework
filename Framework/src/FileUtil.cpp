#include "FileUtil.h"

<<<<<<< HEAD
/// <summary>
/// 文字列置換
/// </summary>
/// <param name="Input"></param>
/// <param name="PatternStr"></param>
/// <param name="ReplaceStr"></param>
/// <returns></returns>
const std::string Replace(
    const std::string& Input,
    std::string PatternStr,
    std::string ReplaceStr)
{
    std::string Result = Input;
    auto Pos = Result.find(PatternStr);

    while (Pos != std::string::npos)
    {
        Result.replace(Pos, PatternStr.length(), ReplaceStr);
        Pos = Result.find(PatternStr, Pos + ReplaceStr.length());
    }

    return Result;
}

const std::wstring Replace(
    const std::wstring& Input,
    std::wstring PatternStr,
    std::wstring ReplaceStr)
{
    std::wstring Result = Input;
    auto Pos = Result.find(PatternStr);

    while (Pos != std::string::npos)
    {
        Result.replace(Pos, PatternStr.length(), ReplaceStr);
        Pos = Result.find(PatternStr, Pos + ReplaceStr.length());
    }

    return Result;
}

const bool SearchFilePathA(const char* filename, std::string& result)
{
    // 無効なファイルを除外
    if (!filename)
=======
const bool SearchFilePahtA(const char* FileName, std::string& Result)
{
    // ファイル名がない場合は見つけられない
    if (!FileName)
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
    {
        return false;
    }

<<<<<<< HEAD
    if (strcmp(filename, " ") == 0 || strcmp(filename, "") == 0)
=======
    if (!strcmp(FileName, " ") || !strcmp(FileName, ""))
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
    {
        return false;
    }

<<<<<<< HEAD
    char exePath[512] = {};
    GetModuleFileNameA(nullptr, exePath, 512);
    exePath[511] = '\0';
    PathRemoveFileSpecA(exePath);

    char dstPath[512] = {};

    strcpy_s(dstPath, filename);
    if (PathFileExistsA(dstPath) == TRUE)
    {
        result = dstPath;
        return true;
    }

    sprintf_s(dstPath, "..\\%s", filename);
    if (PathFileExistsA(dstPath) == TRUE)
    {
        result = dstPath;
        return true;
    }

    sprintf_s(dstPath, "..\\..\\%s", filename);
    if (PathFileExistsA(dstPath) == TRUE)
    {
        result = dstPath;
        return true;
    }

    sprintf_s(dstPath, "\\res\\%s", filename);
    if (PathFileExistsA(dstPath) == TRUE)
    {
        result = dstPath;
        return true;
    }

    sprintf_s(dstPath, "%s\\%s", exePath, filename);
    if (PathFileExistsA(dstPath) == TRUE)
    {
        result = dstPath;
        return true;
    }

    sprintf_s(dstPath, "%s\\..\\%s", exePath, filename);
    if (PathFileExistsA(dstPath) == TRUE)
    {
        result = dstPath;
        return true;
    }

    sprintf_s(dstPath, "%s\\..\\..\\%s", exePath, filename);
    if (PathFileExistsA(dstPath) == TRUE)
    {
        result = dstPath;
        return true;
    }

    sprintf_s(dstPath, "%s\\res\\%s", exePath, filename);
    if (PathFileExistsA(dstPath) == TRUE)
    {
        result = dstPath;
=======
    // 親フォルダ取得
    char ExePath[520] = {};
    GetModuleFileNameA(nullptr, ExePath, 520);
    ExePath[519] = L'\0';
    PathRemoveFileSpecA(ExePath);

    char DistPath[520] = {};
    strcpy_s(DistPath, FileName);
    if (PathFileExistsA(DistPath))
    {
        Result = DistPath;
        return true;
    }

    sprintf_s(DistPath, "..\\%s", FileName);
    if (PathFileExistsA(DistPath))
    {
        Result = DistPath;
        return true;
    }

    sprintf_s(DistPath, "..\\..\\%s", FileName);
    if (PathFileExistsA(DistPath))
    {
        Result = DistPath;
        return true;
    }

    sprintf_s(DistPath, "\\res\\%s", FileName);
    if (PathFileExistsA(DistPath))
    {
        Result = DistPath;
        return true;
    }

    // %EXE_DIR%
    sprintf_s(DistPath, "%s\\%s", ExePath, FileName);
    if (PathFileExistsA(DistPath))
    {
        Result = DistPath;
        return true;
    }


    sprintf_s(DistPath, "%s\\..\\%s", ExePath, FileName);
    if (PathFileExistsA(DistPath))
    {
        Result = DistPath;
        return true;
    }

    sprintf_s(DistPath, "%s\\..\\..\\%s", ExePath, FileName);
    if (PathFileExistsA(DistPath))
    {
        Result = DistPath;
        return true;
    }

    sprintf_s(DistPath, "%s\\res\\%s", ExePath, FileName);
    if (PathFileExistsA(DistPath))
    {
        Result = DistPath;
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
        return true;
    }

    return false;
}

<<<<<<< HEAD
const bool SearchFilePathW(const wchar_t* filename, std::wstring& result)
{
    // 無効なファイルを除外
    if (!filename)
=======
const bool SearchFilePathW(const wchar_t* FileName, std::wstring& Result)
{
    // ファイル名がない場合は見つけられない
    if (!FileName)
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
    {
        return false;
    }

<<<<<<< HEAD
    if (wcscmp(filename, L" ") == 0 || wcscmp(filename, L"") == 0)
=======
    if (!wcscmp(FileName, L" ") || !wcscmp(FileName, L""))
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
    {
        return false;
    }

<<<<<<< HEAD
    wchar_t exePath[512] = {};
    GetModuleFileNameW(nullptr, exePath, 512);
    exePath[511] = '\0';
    PathRemoveFileSpecW(exePath);

    wchar_t dstPath[512] = {};

    wcscpy_s(dstPath, filename);
    if (PathFileExistsW(dstPath) == TRUE)
    {
        result = dstPath;
        return true;
    }

    swprintf_s(dstPath, L"..\\%s", filename);
    if (PathFileExistsW(dstPath) == TRUE)
    {
        result = dstPath;
        return true;
    }

    swprintf_s(dstPath, L"..\\..\\%s", filename);
    if (PathFileExistsW(dstPath) == TRUE)
    {
        result = dstPath;
        return true;
    }

    swprintf_s(dstPath, L"\\res\\%s", filename);
    if (PathFileExistsW(dstPath) == TRUE)
    {
        result = dstPath;
        return true;
    }

    swprintf_s(dstPath, L"%s\\%s", exePath, filename);
    if (PathFileExistsW(dstPath) == TRUE)
    {
        result = dstPath;
        return true;
    }

    swprintf_s(dstPath, L"%s\\..\\%s", exePath, filename);
    if (PathFileExistsW(dstPath) == TRUE)
    {
        result = dstPath;
        return true;
    }

    swprintf_s(dstPath, L"%s\\..\\..\\%s", exePath, filename);
    if (PathFileExistsW(dstPath) == TRUE)
    {
        result = dstPath;
        return true;
    }

    swprintf_s(dstPath, L"%s\\res\\%s", exePath, filename);
    if (PathFileExistsW(dstPath) == TRUE)
    {
        result = dstPath;
=======
    // 親フォルダ取得
    wchar_t ExePath[520] = {};
    GetModuleFileNameW(nullptr, ExePath, 520);
    ExePath[519] = L'\0';
    PathRemoveFileSpecW(ExePath);

    wchar_t DistPath[520] = {};
    wcscpy_s(DistPath, FileName);
    if(PathFileExistsW(DistPath))
    {
        Result = DistPath;
        return true;
    }

    swprintf_s(DistPath, L"..\\%s", FileName);
    if(PathFileExistsW(DistPath))
    {
        Result = DistPath;
        return true;
    }

    swprintf_s(DistPath, L"..\\..\\%s", FileName);
    if (PathFileExistsW(DistPath))
    {
        Result = DistPath;
        return true;
    }

    swprintf_s(DistPath, L"\\res\\%s", FileName);
    if (PathFileExistsW(DistPath))
    {
        Result = DistPath;
        return true;
    }

    // %EXE_DIR%
    swprintf_s(DistPath, L"%s\\%s", ExePath, FileName);
    if (PathFileExistsW(DistPath))
    {
        Result = DistPath;
        return true;
    }


    swprintf_s(DistPath, L"%s\\..\\%s", ExePath, FileName);
    if (PathFileExistsW(DistPath))
    {
        Result = DistPath;
        return true;
    }

    swprintf_s(DistPath, L"%s\\..\\..\\%s", ExePath, FileName);
    if (PathFileExistsW(DistPath))
    {
        Result = DistPath;
        return true;
    }

    swprintf_s(DistPath, L"%s\\res\\%s", ExePath, FileName);
    if (PathFileExistsW(DistPath))
    {
        Result = DistPath;
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
        return true;
    }

    return false;
<<<<<<< HEAD
}

std::string GetFileNameA(const std::string& Path)
{
    auto Temp = Replace(Path, "\\", "/");
    auto Pos = Temp.rfind('/');

    if (Pos != std::string::npos)
    {
        return Temp.substr(Pos + 1);
    }

    return Path;
}


std::wstring GetFileNameW(const std::wstring& Path)
{
    auto Temp = Replace(Path, L"\\", L"/");
    auto Pos = Temp.rfind(L'/');

    if (Pos != std::wstring::npos)
    {
        return Temp.substr(Pos + 1);
    }

    return Path;
}

std::string GetDirectoryNameA(const char* path)
{
    std::string Path = Replace(Path, "\\", "/");
    size_t Index = Path.find_last_of("/");

    if (Index != std::string::npos)
    {
        return Path.substr(0, Index + 1);
    }

    return std::string();
}

std::wstring GetDirectoryNameW(const wchar_t* path)
{
    std::wstring Path = Replace(Path, L"\\", L"/");
    size_t Index = Path.find_last_of(L"/");

    if (Index != std::wstring::npos)
    {
        return Path.substr(0, Index + 1);
    }

    return std::wstring();
}

=======
}
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
