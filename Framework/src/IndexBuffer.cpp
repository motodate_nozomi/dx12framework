#include "IndexBuffer.h"
#include <Logger.h>

IndexBuffer::IndexBuffer()
{

}

IndexBuffer::~IndexBuffer()
{
    Terminate();
}

const bool IndexBuffer::Initialize(
    ID3D12Device* const Device,
    size_t BufferSize,
    const uint32_t* InitData)
{
    // 引数の有効性を検証
    if (!Device)
    {
        ELOG("IndexBuffer::Initialize >> Device が nullptrです.");
        return false;
    }

    // ヒープ設定
    D3D12_HEAP_PROPERTIES HeapProp = {};
    HeapProp.Type = D3D12_HEAP_TYPE_UPLOAD;
    HeapProp.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
    HeapProp.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
    HeapProp.CreationNodeMask = 1;
    HeapProp.VisibleNodeMask = 1;

    // リソース設定
    D3D12_RESOURCE_DESC ResrcDesc = {};
    ResrcDesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
    ResrcDesc.Alignment = 0;
    ResrcDesc.Width = UINT64(BufferSize);
    ResrcDesc.Height = 1;
    ResrcDesc.DepthOrArraySize = 1;
    ResrcDesc.MipLevels = 1;
    ResrcDesc.Format = DXGI_FORMAT_UNKNOWN;
    ResrcDesc.SampleDesc.Count = 1;
    ResrcDesc.SampleDesc.Quality = 0;
    ResrcDesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
    ResrcDesc.Flags = D3D12_RESOURCE_FLAG_NONE;

    // リソースを生成
    auto HR = Device->CreateCommittedResource(
        &HeapProp,
        D3D12_HEAP_FLAG_NONE,
        &ResrcDesc,
        D3D12_RESOURCE_STATE_GENERIC_READ,
        nullptr,
        IID_PPV_ARGS(Resource.GetAddressOf()));

    if (FAILED(HR))
    {
        ELOG("IndexBuffer::Initialize >> リソースの生成に失敗.");
        return false;
    }

    // ビュー作成
    View.BufferLocation = Resource->GetGPUVirtualAddress();
    View.Format = DXGI_FORMAT_R32_UINT;
    View.SizeInBytes = UINT(BufferSize);

    // 初期化データの書き込み
    if (!InitData)
    {
        void* MappedPtr = Map();
        if (!MappedPtr)
        {
            ELOG("IndexBuffer::Initialize >> メモリマッピングに失敗.");
            return false;
        }

        memcpy(MappedPtr, InitData, BufferSize);

        Unmap();
    }

    return true;
}

void IndexBuffer::Terminate()
{
    if (Resource)
    {
        Resource.Reset();
    }
    memset(&View, 0, sizeof(View));
}

uint32_t* const IndexBuffer::Map()
{
    if (!Resource)
    {
        ELOG("IndexBuffer::Map >> メモリマッピングに失敗.");
        return nullptr;
    }

    uint32_t* Ptr = nullptr;
    auto HR = Resource->Map(0, nullptr, reinterpret_cast<void**>(&Ptr));
    if (FAILED(HR))
    {
        return nullptr;
    }

    return Ptr;
}

void IndexBuffer::Unmap()
{
    if (Resource)
    {
        Resource->Unmap(0, nullptr);
    }
}

const D3D12_INDEX_BUFFER_VIEW IndexBuffer::GetView() const
{
    return View;
}
