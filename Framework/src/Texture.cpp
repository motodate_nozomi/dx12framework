#include <Texture.h>
#include <DDSTextureLoader.h>

#include <DescriptorPool.h>
#include <Logger.h>

Texture::Texture()
{

}

Texture::~Texture()
{
    Terminate();
}

const bool Texture::Initialize(ID3D12Device* Device, 
    DescriptorPool* Pool, 
    const wchar_t* Filename, 
    DirectX::ResourceUploadBatch& Batch)
{
    if (!Device)
    {
        ELOG("Texture::Initialize >> Device が nullptr.");
        return false;
    }

    if (!Pool)
    {
        ELOG("Texture::Initialize >> Pool が nullptr.");
        return false;
    }

    if (!Filename)
    {
        ELOG("Texture::Initialize >> Filename が nullptr.");
        return false;
    }

    // プールを設定
    this->Pool = Pool;
    this->Pool->AddRef();

    // ハンドル設定
    Handle = this->Pool->AllocHandle();
    if (!Handle)
    {
        ELOG("Texture::Initialize >> ハンドルの割り当てに失敗.");
        return false;
    }

    // テクスチャ生成
    bool bIsCube = false;
    auto HR = DirectX::CreateDDSTextureFromFile(
        Device,
        Batch,
        Filename,
        Resource.GetAddressOf(),
        true,
        0,
        nullptr,
        &bIsCube);
    if (FAILED(HR))
    {
        ELOG("Texture::Initialize >> DDSテクスチャの生成に失敗.");
        return false;
    }

    // SRVの設定を取得
    auto ViewDesc = GetViewDesc(bIsCube);

    // SRV生成
    Device->CreateShaderResourceView(Resource.Get(), &ViewDesc, Handle->CPUHandle);

    return true;
}

const bool Texture::Initialize(
    ID3D12Device* Device, 
    DescriptorPool* Pool, 
    const D3D12_RESOURCE_DESC* Desc, 
    const bool bIsCube)
{
    if (!Device)
    {
        ELOG("Texture::Initialize >> Device が nullptr.");
        return false;
    }

    if (!Pool)
    {
        ELOG("Texture::Initialize >> Pool が nullptr.");
        return false;
    }

    if (!Desc)
    {
        ELOG("Texture::Initialize >> Desc が nullptr.");
        return false;
    }

    // プールを設定
    this->Pool = Pool;
    this->Pool->AddRef();

    // ハンドル設定
    Handle = this->Pool->AllocHandle();
    if (!Handle)
    {
        ELOG("Texture::Initialize >> ハンドルの割り当てに失敗.");
        return false;
    }

    // ヒープコンフィグ
    D3D12_HEAP_PROPERTIES HeapProp = {};
    HeapProp.Type = D3D12_HEAP_TYPE_DEFAULT;
    HeapProp.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
    HeapProp.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
    HeapProp.CreationNodeMask = 0;
    HeapProp.VisibleNodeMask = 0;

    // リソース作成
    auto HR = Device->CreateCommittedResource(
        &HeapProp,
        D3D12_HEAP_FLAG_NONE,
        Desc,
        D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE,
        nullptr,
        IID_PPV_ARGS(Resource.GetAddressOf()));

    // シェーダリソースビューの設定を求める.
    auto viewDesc = GetViewDesc(bIsCube);

    // シェーダリソースビューを生成します.
    Device->CreateShaderResourceView(Resource.Get(), &viewDesc, Handle->CPUHandle);


    return true;
}

void Texture::Terminate()
{
    Resource.Reset();

    if (this->Pool && Handle)
    {
        this->Pool->FreeHandle(Handle);
        Handle = nullptr;

        this->Pool->Release();
        this->Pool = nullptr;
    }
}

const D3D12_CPU_DESCRIPTOR_HANDLE Texture::GetCPUHandle() const
{
    if (Handle)
    {
        return Handle->CPUHandle;
    }

    return D3D12_CPU_DESCRIPTOR_HANDLE();
}

const D3D12_GPU_DESCRIPTOR_HANDLE Texture::GetGPUHandle() const
{
    if (Handle)
    {
        return Handle->GPUHandle;
    }

    return D3D12_GPU_DESCRIPTOR_HANDLE();
}

D3D12_SHADER_RESOURCE_VIEW_DESC Texture::GetViewDesc(const bool bIsCube)
{
    if (!Resource)
    {
        DLOG("Texture::GetViewDesc >> テクスチャリソースがありません.");
        return D3D12_SHADER_RESOURCE_VIEW_DESC();
    }

    auto RrcDesc = Resource->GetDesc();
    D3D12_SHADER_RESOURCE_VIEW_DESC ViewDesc = {};
    ViewDesc.Format = RrcDesc.Format;
    ViewDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;

    switch (RrcDesc.Dimension)
    {
        case D3D12_RESOURCE_DIMENSION_BUFFER:
        {
            ELOG("Texture::GetViewDesc >> バッファは対象外です.");
            abort();
            break;
        }

        case D3D12_RESOURCE_DIMENSION_TEXTURE1D:
        {
            if (RrcDesc.DepthOrArraySize > 1)
            {
                ViewDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE1DARRAY;
                ViewDesc.Texture1DArray.MostDetailedMip = 0;
                ViewDesc.Texture1DArray.MipLevels = RrcDesc.MipLevels;
                ViewDesc.Texture1DArray.FirstArraySlice = 0;
                ViewDesc.Texture1DArray.ArraySize = RrcDesc.DepthOrArraySize;
                ViewDesc.Texture1DArray.ResourceMinLODClamp = 0.0f;
            }
            else
            {
                ViewDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE1D;
                ViewDesc.Texture1D.MostDetailedMip = 0;
                ViewDesc.Texture1D.MipLevels = RrcDesc.MipLevels;
                ViewDesc.Texture1D.ResourceMinLODClamp = 0.0f;
            }

            break;
        }

        case D3D12_RESOURCE_DIMENSION_TEXTURE2D:
        {
            if (bIsCube)
            {

            }
            else
            {
                if (RrcDesc.DepthOrArraySize > 1)
                {

                }
                else
                {
                    if (RrcDesc.MipLevels > 1)
                    {
                        ViewDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2DMS;
                    }
                    else
                    {
                        ViewDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
                        ViewDesc.Texture2D.MostDetailedMip = 0;
                        ViewDesc.Texture2D.MipLevels = RrcDesc.MipLevels;
                        ViewDesc.Texture2D.PlaneSlice = 0;
                        ViewDesc.Texture2D.ResourceMinLODClamp = 0.0f;
                    }
                }
            }

            break;
        }

        case D3D12_RESOURCE_DIMENSION_TEXTURE3D:
        {
            break;
        }

        // 想定外
        default:
        {
            ELOG("Texture::GetViewDesc >> リソースの次元が不正です.");
            abort();
            break;
        }
    }

    return ViewDesc;
}
