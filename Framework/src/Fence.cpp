#include "Fence.h"
#include <Logger.h>

Fence::Fence()
{

}

Fence::~Fence()
{
    Terminate();
}

const bool Fence::Initialize(ID3D12Device* const Device)
{
    if (!Device)
    {
        ELOG("Fence::Initialize >> Device が nullptr.");
        return false;
    }

    // イベント生成
    Event = CreateEventEx(nullptr, FALSE, FALSE, EVENT_ALL_ACCESS);
    if (!Event)
    {
        ELOG("Fence::Initialize >> イベントの生成に失敗.");
        return false;
    }

    // フェンス生成
    auto HR = Device->CreateFence(
        0,
        D3D12_FENCE_FLAG_NONE,
        IID_PPV_ARGS(D3D12Fence.GetAddressOf()));

    if (FAILED(HR))
    {
        ELOG("Fence::Initialize >> フェンスの生成に失敗.");
        return false;
    }

    Counter = 1;

    return true;
}

void Fence::Terminate()
{
    if (Event)
    {
        CloseHandle(Event);
        Event = nullptr;
    }

    D3D12Fence.Reset();
    D3D12Fence = nullptr;

    Counter = 0;
}

void Fence::Wait(ID3D12CommandQueue* const Queue, UINT Timeout)
{
    if (!Queue)
    {
        ELOG("Fence::Wait >> CmdQueue が nullptr.");
        return;
    }

    const auto FenceValue = Counter;
    auto HR = Queue->Signal(D3D12Fence.Get(), FenceValue);
    if (FAILED(HR))
    {
        ELOG("Fence::Wait >> シグナル処理に失敗.");
        return;
    }

    ++Counter;

    // GPU処理が終わってないなら、待機
    if(D3D12Fence->GetCompletedValue() < FenceValue)
    { 
        auto HR = D3D12Fence->SetEventOnCompletion(FenceValue, Event);
        if (FAILED(HR))
        {
            return;
        }

        if (WAIT_OBJECT_0 != WaitForSingleObjectEx(Event, Timeout, FALSE))
        {
            return;
        }
    }

}

void Fence::Sync(ID3D12CommandQueue* const Queue)
{
    if (!Queue)
    {
        ELOG("Fence::Sync >> CmdQueue が nullptr.");
        return;
    }

    // Counter : GPUコマンド完了時に設定される値
    auto HR = Queue->Signal(D3D12Fence.Get(), Counter);
    if (FAILED(HR))
    {
        ELOG("Fence::Wait >> シグナル処理に失敗.");
        return;
    }

    // シグナル状態になったら実行される処理を設定
    HR = D3D12Fence->SetEventOnCompletion(Counter, Event);
    if (FAILED(HR))
    {
        return;
    }

    // 待機
    if (WAIT_OBJECT_0 != WaitForSingleObjectEx(Event, INFINITE, FALSE))
    {
        return;
    }

    // 次の値として使えるように増やす
    ++Counter;
}
