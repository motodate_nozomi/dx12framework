#include <cassert>

#include "App.h"
#include "FileUtil.h"

// DXTK
#include "ResourceUploadBatch.h"
#include "DDSTextureLoader.h"
#include "VertexTypes.h"


#include <cassert>

#include "ResourceUploadBatch.h"
#include "DDSTextureLoader.h"
#include "VertexTypes.h"
#include "FileUtil.h"
#include "DescriptorPool.h"

// Utility
namespace {
    const auto ClassName = TEXT("Window");

    template<typename T>
    void SafeRelease(T*& Ptr)
    {
        if (Ptr)
        {
            Ptr->Release();
            Ptr = nullptr;
        }
    }

    struct Vertex
    {
        DirectX::XMFLOAT3 Position;
        DirectX::XMFLOAT2 UV;
    };

} // end of namespace

App::App(const uint32_t Width, const uint32_t Height)
    : HInstance(nullptr)
    , HWnd(nullptr)
    , Width(Width)
    , Height(Height)
{}

App::~App()
{

}

void App::Run()
{
    if (InitializeApp())
    {
        MainLoop();
    }

    TerminateApp();
}

const bool App::InitializeApp()
{
    if (!InitializeWindow())
    {
        return false;
    }

    if (!InitializeD3D())
    {
        return false;
    }

    if (!OnInitialize())
    {
        return false;
    }

    return true;
}

void App::TerminateApp()
{
    // Delete AppData
    OnTerminate();

    // Delete D3D
    TerminateD3D();

    // Delete Window
    TerminateWindow();
}

const bool App::InitializeWindow()
{
    // get instanceHandle
    auto HInst = GetModuleHandle(nullptr);
    if (!HInst)
    {
        OutputDebugStringW(L"Not find HInstance.\n");
        return false;
    }

    // set windowConf
    WNDCLASSEX WC = {};
    WC.cbSize = sizeof(WNDCLASSEX);
    WC.style = CS_HREDRAW | CS_VREDRAW;
    WC.lpfnWndProc = WndProc;
    WC.hIcon = LoadIcon(HInst, IDI_APPLICATION);
    WC.hCursor = LoadCursor(HInst, IDC_ARROW);
    WC.hbrBackground = GetSysColorBrush(COLOR_BACKGROUND);
    WC.lpszMenuName = nullptr;
    WC.lpszClassName = ClassName;
    WC.hIconSm = LoadIcon(HInst, IDI_APPLICATION);

    // register window
    if (!RegisterClassEx(&WC))
    {
        OutputDebugStringW(L"Failed Register Window.\n");
        return false;
    }

    this->HInstance = HInst;

    // set windowSize
    RECT RC = {};
    RC.right = static_cast<LONG>(Width);
    RC.bottom = static_cast<LONG>(Height);

    // adjust windowSize
    auto Style = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU;
    AdjustWindowRect(&RC, Style, FALSE);

    // create window
    HWnd = CreateWindowEx(
        0,
        ClassName,
        TEXT("Framework"),
        Style,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        RC.right - RC.left,
        RC.bottom - RC.top,
        nullptr,
        nullptr,
        HInstance,
        nullptr);

    if (!HWnd)
    {
        OutputDebugStringW(L"Failed Create Window.\n");
        return false;
    }

    ShowWindow(HWnd, SW_SHOWNORMAL);

    UpdateWindow(HWnd);

    SetFocus(HWnd);

    return true;
}

void App::TerminateWindow()
{
    if (HInstance)
    {
        UnregisterClass(ClassName, HInstance);
    }

    HInstance = nullptr;
    HWnd = nullptr;
}

void App::MainLoop()
{
    MSG Msg = {};

    while (WM_QUIT != Msg.message)
    {
        if (PeekMessage(&Msg, nullptr, 0, 0, PM_REMOVE) == TRUE)
        {
            TranslateMessage(&Msg);
            DispatchMessage(&Msg);
        }
        else
        {
            Render();
        }
    }
}

bool App::InitializeD3D()
{
    // Enable DebugLayer
    #if defined(DEBUG) || defined(_DEBUG)
    {
        ComPtr<ID3D12Debug> Debug = nullptr;
        auto HR = D3D12GetDebugInterface(IID_PPV_ARGS(Debug.GetAddressOf()));
        if (SUCCEEDED(HR))
        {
            Debug->EnableDebugLayer();
        }
    }
    #endif

    // create device
    auto HR = D3D12CreateDevice(
        nullptr,
        D3D_FEATURE_LEVEL_11_0,
        IID_PPV_ARGS(D3D12Device.GetAddressOf())
    );
    
    // create commandQueue
    {
        D3D12_COMMAND_QUEUE_DESC Desc = {};
        Desc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
        Desc.Priority = D3D12_COMMAND_QUEUE_PRIORITY_NORMAL;
        Desc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
        Desc.NodeMask = 0;

        HR = D3D12Device->CreateCommandQueue(&Desc, IID_PPV_ARGS(CommandQueue.GetAddressOf()));
    }

    // create swapChain
    {
        ComPtr<IDXGIFactory4> Factory = nullptr;
        HR = CreateDXGIFactory1(IID_PPV_ARGS(Factory.GetAddressOf()));
        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed create dxgiFactory for swapChain.");
            return false;
        }

        DXGI_SWAP_CHAIN_DESC Desc = {};
        Desc.BufferDesc.Width = this->Width;
        Desc.BufferDesc.Height = this->Height;
        Desc.BufferDesc.RefreshRate.Numerator = 60;
        Desc.BufferDesc.RefreshRate.Denominator = 1;
        Desc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
        Desc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
        Desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
        Desc.SampleDesc.Count = 1;
        Desc.SampleDesc.Quality = 0;
        Desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
        Desc.BufferCount = FrameCount;
        Desc.OutputWindow = HWnd;
        Desc.Windowed = TRUE;
        Desc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
        Desc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

        ComPtr<IDXGISwapChain> TempSwapChain = nullptr;
        HR = Factory->CreateSwapChain(CommandQueue.Get(), &Desc, TempSwapChain.GetAddressOf());
        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed create TempswapChain.\n");
            return false;
        }

        HR = TempSwapChain.As(&SwapChain);
        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed create swapChain3.\n");
            return false;
        }

        // get buckBufferNum
        FrameIndex = SwapChain->GetCurrentBackBufferIndex();

        Factory.Reset();
        TempSwapChain.Reset();

    }

    // create commandAllocator
    {
        for (auto i = 0u; i < FrameCount; ++i)
        {
            HR = D3D12Device->CreateCommandAllocator(
                D3D12_COMMAND_LIST_TYPE_DIRECT,
                IID_PPV_ARGS(&CommandAllocator[i])
            );

            if (FAILED(HR))
            {
                OutputDebugStringW(L"Failed Create CommandAloc.\n");
                return false;
            }
        }
    }

    // create CommandList
    {
        HR = D3D12Device->CreateCommandList(
            0,
            D3D12_COMMAND_LIST_TYPE_DIRECT,
            CommandAllocator[FrameIndex].Get(),
            nullptr,
            IID_PPV_ARGS(&CommandList)
        );

        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed Create CommandList.\n");
            return false;
        }
    }

    // create RTV
    {
        D3D12_DESCRIPTOR_HEAP_DESC Desc = {};
        Desc.NumDescriptors = FrameCount;
        Desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
        Desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
        Desc.NodeMask = 0;

        /*       DescriptorPool* TestPool;
               DescriptorPool::Create(D3D12Device.Get(), &Desc, &TestPool);
               HeapRTV = TestPool->GetHeap();*/

        HR = D3D12Device->CreateDescriptorHeap(&Desc, IID_PPV_ARGS(&HeapRTV));
        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed Create RTV.\n");
            return false;
        }

        auto Handle = HeapRTV->GetCPUDescriptorHandleForHeapStart();
        auto incrementSize = D3D12Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

        for (auto i = 0u; i < FrameCount; ++i)
        {
            HR = SwapChain->GetBuffer(i, IID_PPV_ARGS(ColorBuffer[i].GetAddressOf()));
            if (FAILED(HR))
            {
                OutputDebugStringW(L"Failed to get ColorBuffer.\n");
                return false;
            }

            D3D12_RENDER_TARGET_VIEW_DESC ViewDesc = {};
            ViewDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
            ViewDesc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE2D;
            ViewDesc.Texture2D.MipSlice = 0;
            ViewDesc.Texture2D.PlaneSlice = 0;

            D3D12Device->CreateRenderTargetView(ColorBuffer[i].Get(), &ViewDesc, Handle);
            HandleRTV[i] = Handle;
            Handle.ptr += incrementSize;
 
        }
    }

    // Create DepthStencilBuffer
    {
        D3D12_HEAP_PROPERTIES Prop = {};
        Prop.Type = D3D12_HEAP_TYPE_DEFAULT;
        Prop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
        Prop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
        Prop.CreationNodeMask = 1;
        Prop.VisibleNodeMask = 1;

        D3D12_RESOURCE_DESC ResDesc = {};
        ResDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
        ResDesc.Alignment = 0;
        ResDesc.Width = Width;
        ResDesc.Height = Height;
        ResDesc.DepthOrArraySize = 1;
        ResDesc.MipLevels = 1;
        ResDesc.Format = DXGI_FORMAT_D32_FLOAT;
        ResDesc.SampleDesc.Count = 1;
        ResDesc.SampleDesc.Quality = 0;
        ResDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
        ResDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

        D3D12_CLEAR_VALUE ClearValue = {};
        ClearValue.Format = DXGI_FORMAT_D32_FLOAT;
        ClearValue.DepthStencil.Depth = 1.0f;
        ClearValue.DepthStencil.Stencil = 0;

        HR = D3D12Device->CreateCommittedResource(
            &Prop,
            D3D12_HEAP_FLAG_NONE,
            &ResDesc,
            D3D12_RESOURCE_STATE_DEPTH_WRITE,
            &ClearValue,
            IID_PPV_ARGS(DepthStencilBuffer.GetAddressOf())
        );

        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed create DepthStencilBuffer.\n");
            return false;
        }

        // Create DescriptorHeap
        D3D12_DESCRIPTOR_HEAP_DESC HeapDesc = {};
        HeapDesc.NumDescriptors = 1;
        HeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
        HeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
        HeapDesc.NodeMask = 0;

        HR = D3D12Device->CreateDescriptorHeap(
            &HeapDesc,
            IID_PPV_ARGS(HeapDSV.GetAddressOf())
        );

        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed create HeapDSV for DepthStencilBuffer.\n");
            return false;
        }

        auto Handle = HeapDSV->GetCPUDescriptorHandleForHeapStart();
        auto IncrementSize = D3D12Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);

        D3D12_DEPTH_STENCIL_VIEW_DESC ViewDesc = {};
        ViewDesc.Format = DXGI_FORMAT_D32_FLOAT;
        ViewDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
        ViewDesc.Texture2D.MipSlice = 0;
        ViewDesc.Flags = D3D12_DSV_FLAG_NONE;

        D3D12Device->CreateDepthStencilView(DepthStencilBuffer.Get(), &ViewDesc, Handle);

        HandleDSV = Handle;
    }

    // create Fence
    {
        for (auto i = 0u; i < FrameCount; ++i)
        {
            FenceCounter[i] = 0;
        }

        HR = D3D12Device->CreateFence(
            FenceCounter[FrameIndex],
            D3D12_FENCE_FLAG_NONE,
            IID_PPV_ARGS(Fence.GetAddressOf())
        );

        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed crate fence.\n");
            return false;
        }

        FenceCounter[FrameIndex]++;

        // generate event
        FenceEvent = CreateEvent(nullptr, FALSE, FALSE, nullptr);
        if (!FenceEvent)
        {
            OutputDebugStringW(L"Failed crate fenceEvenet.\n");
            return false;
        }
    }

    CommandList->Close();

    return true;
}

void App::TerminateD3D()
{
    WaitGPU();

    // イベント破棄
    if (FenceEvent)
    {
        CloseHandle(FenceEvent);
        FenceEvent = nullptr;
    }

    // フェンス削除
    Fence.Reset();

    // RTV削除
    HeapRTV.Reset();
    for (auto i = 0u; i < FrameCount; ++i)
    {
        ColorBuffer[i].Reset();
        ColorBuffer[i] = nullptr;
    }

    // コマンドリスト削除
    CommandList.Reset();

    // コマンドアロケータ削除
    for (auto i = 0u; i < FrameCount; ++i)
    {
        CommandAllocator[i].Reset();
    }

    // スワップチェイン削除
    SwapChain.Reset();

    CommandQueue.Reset();

    D3D12Device.Reset();
}

const bool App::OnInitialize()
{
<<<<<<< HEAD
    // メッシュ読み込み
    {
        std::wstring MeshPath = {};
        if (!SearchFilePath(L"res/teapot/teapot.obj" /*L"res/Test.fbx"*/, MeshPath))
        {
            OutputDebugStringW(L"メッシュデータが見つかりません。 \n");
            return false;
        }

        if (!LoadMesh(MeshPath.c_str(), StaticMeshes, StaticMeshMaterials))
        {
            OutputDebugStringW(L"メッシュの読み込みに失敗しました。 \n");
            return false;
        }
=======
    // Load Mesh
    {
        std::wstring Path = {};
        if(!SearchFilePath(L"res/teapot/teapot.obj", Path))
        {
            OutputDebugStringW(L"Not find mesh's file. \n");
            return false;
        }

        if (!LoadMesh(Path.c_str(), Meshes, Materials))
        {
            OutputDebugStringW(L"Failed to loading mesh.\n");
            return false;
        }

        // limit meshNum
        assert(Meshes.size() == 1);
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
    }

    // generate vb
    {
<<<<<<< HEAD
        //Vertex Verteces[] = {
        //    {DirectX::XMFLOAT3(-1.0f, 1.0f, 0.0f), DirectX::XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f) },
        //    {DirectX::XMFLOAT3(1.0f, 1.0f, 0.0f), DirectX::XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f) },
        //    {DirectX::XMFLOAT3(1.0f, -1.0f, 0.0f), DirectX::XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },
        //    {DirectX::XMFLOAT3(-1.0f, -1.0f, 0.0f), DirectX::XMFLOAT4(1.0f, 0.0f, 1.0f, 1.0f) },
        //};

        // 3, 4行目が逆かも
        //DirectX::VertexPositionTexture Vertices[] = {
        //    DirectX::VertexPositionTexture(DirectX::XMFLOAT3(-1.0f, 1.0f, 0.0f), DirectX::XMFLOAT2(0.0f, 0.0f)),
        //    DirectX::VertexPositionTexture(DirectX::XMFLOAT3( 1.0f, 1.0f, 0.0f), DirectX::XMFLOAT2(1.0f, 0.0f)),
        //    DirectX::VertexPositionTexture(DirectX::XMFLOAT3(1.0f, -1.0f, 0.0f), DirectX::XMFLOAT2(1.0f, 1.0f)),
        //    DirectX::VertexPositionTexture(DirectX::XMFLOAT3(-1.0f, -1.0f, 0.0f), DirectX::XMFLOAT2(0.0f, 1.0f)),
        //};

        auto Size = sizeof(MeshVertex) * StaticMeshes[0].Vertices.size();
        auto Vertices = StaticMeshes[0].Vertices.data();

=======
        auto Size = sizeof(MeshVertex) * Meshes[0].Vertices.size();
        auto Vertices = Meshes[0].Vertices.data();
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55

        // heap property
        D3D12_HEAP_PROPERTIES HeapProp = {};
        HeapProp.Type = D3D12_HEAP_TYPE_UPLOAD;
        HeapProp.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
        HeapProp.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
        HeapProp.CreationNodeMask = 1;
        HeapProp.VisibleNodeMask = 1;

        // set resource
        D3D12_RESOURCE_DESC Desc = {};
        Desc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
        Desc.Alignment = 0;
<<<<<<< HEAD
        //Desc.Width = sizeof(Vertices);
=======
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
        Desc.Width = Size;
        Desc.Height = 1;
        Desc.DepthOrArraySize = 1;
        Desc.MipLevels = 1;
        Desc.Format = DXGI_FORMAT_UNKNOWN;
        Desc.SampleDesc.Count = 1;
        Desc.SampleDesc.Quality = 0;
        Desc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
        Desc.Flags = D3D12_RESOURCE_FLAG_NONE;

        // generate resource
        auto HR = D3D12Device->CreateCommittedResource(
            &HeapProp,
            D3D12_HEAP_FLAG_NONE,
            &Desc,
            D3D12_RESOURCE_STATE_GENERIC_READ,
            nullptr,
            IID_PPV_ARGS(VertexBuffer.GetAddressOf()
            ));

        // mapping
        void* Ptr = nullptr;
        HR = VertexBuffer->Map(0, nullptr, &Ptr);
        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed to mapping VertexBuffer. \n");
            return false;
        }

        // assign vb 
<<<<<<< HEAD
        memcpy(Ptr, Vertices, Size);
=======
        memcpy(Ptr, Vertices, sizeof(Size));
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
        // unmap
        VertexBuffer->Unmap(0, nullptr);
        
        // create bufferView
        VBView.BufferLocation = VertexBuffer->GetGPUVirtualAddress();
<<<<<<< HEAD
        //VBView.SizeInBytes = static_cast<UINT>(sizeof(Vertices));
        //VBView.StrideInBytes = static_cast<UINT>(sizeof(DirectX::VertexPositionTexture));
        VBView.SizeInBytes = static_cast<UINT>(Size);
=======
        VBView.SizeInBytes = static_cast<UINT>(sizeof(Size));
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
        VBView.StrideInBytes = static_cast<UINT>(sizeof(MeshVertex));
    }

    // Create IndexBuffer
    {
<<<<<<< HEAD
        //uint32_t Indices[] = {0, 1, 2, 0, 2, 3};

        auto Size = sizeof(uint32_t) * StaticMeshes[0].Indices.size();
        auto Indices = StaticMeshes[0].Indices.data();
=======
        auto Size = sizeof(uint32_t) * Meshes[0].Indices.size();
        auto Indices = Meshes[0].Indices.data();
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55

        // Define HeapProp
        D3D12_HEAP_PROPERTIES Prop = {};
        Prop.Type = D3D12_HEAP_TYPE_UPLOAD;
        Prop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
        Prop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
        Prop.CreationNodeMask = 1;
        Prop.VisibleNodeMask = 1;

        // Set ResourceDesc
        D3D12_RESOURCE_DESC Desc = {};
        Desc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
        Desc.Alignment = 0;
<<<<<<< HEAD
        //Desc.Width = sizeof(Indices);
=======
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
        Desc.Width = Size;
        Desc.Height = 1;
        Desc.DepthOrArraySize = 1;
        Desc.MipLevels = 1;
        Desc.Format = DXGI_FORMAT_UNKNOWN;
        Desc.SampleDesc.Count = 1;
        Desc.SampleDesc.Quality = 0;
        Desc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
        Desc.Flags = D3D12_RESOURCE_FLAG_NONE;

        // Generate Resource
        auto HR = D3D12Device->CreateCommittedResource(
            &Prop,
            D3D12_HEAP_FLAG_NONE,
            &Desc,
            D3D12_RESOURCE_STATE_GENERIC_READ,
            nullptr,
            IID_PPV_ARGS(IndexBuffer.GetAddressOf())
        );

        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed to create IndexBuffer. \n");
            return false;
        }

        // Mapping
        void* Ptr = nullptr;
        HR = IndexBuffer->Map(0, nullptr, &Ptr);
        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed to Mapping for IndexBuffer. \n");
            return false;
        }

        // Set IndexData to MappingData
<<<<<<< HEAD
        memcpy(Ptr, Indices, /*sizeof(Indices)*/ Size);
=======
        memcpy(Ptr, Indices, Size);
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55

        // Unmap
        IndexBuffer->Unmap(0, nullptr);

        // Set IndexBufferView
        IBView.BufferLocation = IndexBuffer->GetGPUVirtualAddress();
        IBView.Format = DXGI_FORMAT_R32_UINT;
        IBView.SizeInBytes = static_cast<UINT>(Size);
    }

    // generate descHeap for CBV/SRV/UAV
    {
        D3D12_DESCRIPTOR_HEAP_DESC Desc = {};
        Desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
        Desc.NumDescriptors = 3;
        Desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
        Desc.NodeMask = 0;

        auto HR = D3D12Device->CreateDescriptorHeap(
            &Desc,
            IID_PPV_ARGS(HeapCBV_SRV_UAV.GetAddressOf())
        );

        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed to create DescHeap for ConstantBuffer. \n");
            return false;
        }
    }

    // Generate ConstantBuffer
    {
        D3D12_HEAP_PROPERTIES Props = {};
        Props.Type = D3D12_HEAP_TYPE_UPLOAD;
        Props.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
        Props.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
        Props.CreationNodeMask = 1;
        Props.VisibleNodeMask = 1;

        // Set Resource
        D3D12_RESOURCE_DESC Desc = {};
        Desc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
        Desc.Alignment = 0;
        Desc.Width = sizeof(Transform);
        Desc.Height = 1;
        Desc.DepthOrArraySize = 1;
        Desc.MipLevels = 1;
        Desc.Format = DXGI_FORMAT_UNKNOWN;
        Desc.SampleDesc.Count = 1;
        Desc.SampleDesc.Quality = 0;
        Desc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
        Desc.Flags = D3D12_RESOURCE_FLAG_NONE;

        auto IncrementSize = D3D12Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
    
        for (auto i = 0; i < FrameCount; ++i)
        {
            // Create Resource
            auto HR = D3D12Device->CreateCommittedResource(
                &Props,
                D3D12_HEAP_FLAG_NONE,
                &Desc,
                D3D12_RESOURCE_STATE_GENERIC_READ,
                nullptr,
                IID_PPV_ARGS(ConstantBuffer[i].GetAddressOf())
            );

            if (FAILED(HR))
            {
                OutputDebugStringW(L"Failed to create Resource. \n");
                return false;
            }

            auto GPUAddress = ConstantBuffer[i]->GetGPUVirtualAddress();
            auto HandleCPU = HeapCBV_SRV_UAV->GetCPUDescriptorHandleForHeapStart();
            auto HandleGPU = HeapCBV_SRV_UAV->GetGPUDescriptorHandleForHeapStart();

            HandleCPU.ptr += IncrementSize * i;
            HandleGPU.ptr += IncrementSize * i;

            CBView[i].HandleCPU = HandleCPU;
            CBView[i].HandleGPU = HandleGPU;
            CBView[i].Desc.BufferLocation = GPUAddress;
            CBView[i].Desc.SizeInBytes = sizeof(Transform);

            // generate ConstatnBufferView
            D3D12Device->CreateConstantBufferView(&CBView[i].Desc, HandleCPU);

            // Mapping
            HR = ConstantBuffer[i]->Map(0, nullptr, reinterpret_cast<void**>(&CBView[i].Buffer));
            if (FAILED(HR))
            {
                OutputDebugStringW(L"Failed to mapping ConstantBuffer. \n");
                return false;
            }

            auto EyePos = DirectX::XMVectorSet(0.0f, 1.0f, 2.0f, 0.0f);
            auto TargetPos = DirectX::XMVectorZero();
            auto Up = DirectX::XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

            auto FovY = DirectX::XMConvertToRadians(37.5f);
            auto Aspect = static_cast<float>(Width) / static_cast<float>(Height);

            // Set TransformMatrix
            CBView[i].Buffer->World = DirectX::XMMatrixIdentity();
            CBView[i].Buffer->View = DirectX::XMMatrixLookAtRH(EyePos, TargetPos, Up);
            CBView[i].Buffer->Proj = DirectX::XMMatrixPerspectiveFovRH(FovY, Aspect, 1.0f, 1000.0f);
        }
    }

    // Create RootSignature
    {
        auto Flag = D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT;
        Flag |= D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS;
        Flag |= D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS;
        Flag |= D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS;

        // Set RootPrm(exist for 32bitData = Param.Constant)
        D3D12_ROOT_PARAMETER Param[2] = {};
        Param[0].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
        Param[0].Descriptor.ShaderRegister = 0;
        Param[0].Descriptor.RegisterSpace = 0;
        Param[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_VERTEX;
        
        D3D12_DESCRIPTOR_RANGE Range = {};
        Range.RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
        Range.NumDescriptors = 1;
        Range.BaseShaderRegister = 0;
        Range.RegisterSpace = 0;
        Range.OffsetInDescriptorsFromTableStart = 0;

        Param[1].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
        Param[1].DescriptorTable.NumDescriptorRanges = 1;
        Param[1].DescriptorTable.pDescriptorRanges = &Range;
        Param[1].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

<<<<<<< HEAD
        // スタティックサンプラーの設定
=======
        // Set StaticSampler
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
        D3D12_STATIC_SAMPLER_DESC Sampler = {};
        Sampler.Filter = D3D12_FILTER_MIN_MAG_MIP_LINEAR;
        Sampler.AddressU = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
        Sampler.AddressV = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
        Sampler.AddressW = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
        Sampler.MipLODBias = D3D12_DEFAULT_MIP_LOD_BIAS;
        Sampler.MaxAnisotropy = 1;
        Sampler.ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;
        Sampler.BorderColor = D3D12_STATIC_BORDER_COLOR_OPAQUE_BLACK;
        Sampler.MinLOD = -D3D12_FLOAT32_MAX;
        Sampler.MaxLOD = +D3D12_FLOAT32_MAX;
        Sampler.ShaderRegister = 0;
        Sampler.RegisterSpace = 0;
        Sampler.ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

        // Set RootSinature
        D3D12_ROOT_SIGNATURE_DESC Desc = {};
<<<<<<< HEAD
        //Desc.NumParameters = 1;
        //Desc.NumStaticSamplers = 0;
        //Desc.pStaticSamplers = nullptr;
        Desc.NumParameters = 2;
        Desc.NumStaticSamplers = 1;
        Desc.pStaticSamplers = &Sampler;
        Desc.pParameters = Param;
=======
        Desc.NumParameters = 2;
        Desc.NumStaticSamplers = 1;
        Desc.pParameters = Param;
        Desc.pStaticSamplers = &Sampler;
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
        Desc.Flags = Flag;

        ComPtr<ID3DBlob> Blob = nullptr;
        ComPtr<ID3DBlob> ErrorBlob = nullptr;

        // Serialize
        auto HR = D3D12SerializeRootSignature(
            &Desc,
            D3D_ROOT_SIGNATURE_VERSION_1_0,
            Blob.GetAddressOf(),
            ErrorBlob.GetAddressOf()
        );

        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed to Serialize.\n");
            return false;
        }

        // Generate RootSignature
        HR = D3D12Device->CreateRootSignature(
            0,
            Blob->GetBufferPointer(),
            Blob->GetBufferSize(),
            IID_PPV_ARGS(RootSignature.GetAddressOf())
        );

        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed to Crate RootSignature.\n");
            return false;
        }
    }

    // Create PSO
    {
        // Assign InputLayout
        D3D12_INPUT_ELEMENT_DESC ElementDescs[2] = {};
        ElementDescs[0].SemanticName = "POSITION";
        ElementDescs[0].SemanticIndex = 0;
        ElementDescs[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
        ElementDescs[0].InputSlot = 0;
        ElementDescs[0].AlignedByteOffset = D3D12_APPEND_ALIGNED_ELEMENT;
        ElementDescs[0].InputSlotClass = D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA;
        ElementDescs[0].InstanceDataStepRate = 0;

<<<<<<< HEAD
        //ElementDescs[1].SemanticName = "COLOR";
        //ElementDescs[1].SemanticIndex = 0;
        //ElementDescs[1].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
        //ElementDescs[1].InputSlot = 0;
        //ElementDescs[1].AlignedByteOffset = D3D12_APPEND_ALIGNED_ELEMENT;
        //ElementDescs[1].InputSlotClass = D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA;
        //ElementDescs[1].InstanceDataStepRate = 0;

=======
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
        ElementDescs[1].SemanticName = "TEXCOORD";
        ElementDescs[1].SemanticIndex = 0;
        ElementDescs[1].Format = DXGI_FORMAT_R32G32_FLOAT;
        ElementDescs[1].InputSlot = 0;
        ElementDescs[1].AlignedByteOffset = D3D12_APPEND_ALIGNED_ELEMENT;
        ElementDescs[1].InputSlotClass = D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA;
        ElementDescs[1].InstanceDataStepRate = 0;

        // Set RasterState
        D3D12_RASTERIZER_DESC RSDesc = {};
        RSDesc.FillMode = D3D12_FILL_MODE_SOLID;
        RSDesc.CullMode = D3D12_CULL_MODE_NONE;
        RSDesc.FrontCounterClockwise = FALSE;
        RSDesc.DepthBias = D3D12_DEFAULT_DEPTH_BIAS;
        RSDesc.DepthBiasClamp = D3D12_DEFAULT_DEPTH_BIAS_CLAMP;
        RSDesc.DepthClipEnable = FALSE;
        RSDesc.MultisampleEnable = FALSE;
        RSDesc.AntialiasedLineEnable = FALSE;
        RSDesc.ForcedSampleCount = 0;
        RSDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;

        // Generate BlendMode For RenderTarget
        D3D12_RENDER_TARGET_BLEND_DESC RTBlendStateDesc = {
            FALSE, FALSE,
            D3D12_BLEND_ONE, D3D12_BLEND_ZERO, D3D12_BLEND_OP_ADD,
            D3D12_BLEND_ONE, D3D12_BLEND_ZERO, D3D12_BLEND_OP_ADD,
            D3D12_LOGIC_OP_NOOP,
            D3D12_COLOR_WRITE_ENABLE_ALL
        };

        // Set BlendState
        D3D12_BLEND_DESC BlendStateDesc = {};
        BlendStateDesc.AlphaToCoverageEnable = FALSE;
        BlendStateDesc.IndependentBlendEnable = FALSE;
        for (UINT i = 0; i < D3D12_SIMULTANEOUS_RENDER_TARGET_COUNT; ++i)
        {
            BlendStateDesc.RenderTarget[i] = RTBlendStateDesc;
        }

        // Set DepthStencilState
        D3D12_DEPTH_STENCIL_DESC DSSDesc = {};
        DSSDesc.DepthEnable = TRUE;
        DSSDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
        DSSDesc.DepthFunc = D3D12_COMPARISON_FUNC_LESS_EQUAL;
        DSSDesc.StencilEnable = FALSE;

        // Load Shader
        ComPtr<ID3DBlob> VSBlob = nullptr;
        ComPtr<ID3DBlob> PSBlob = nullptr;

<<<<<<< HEAD
        // シェーダファイルのパス検索
        std::wstring VSPath = {};
        std::wstring PSPath = {};

        if (!SearchFilePathW(L"SimpleTexVS.cso", VSPath))
        {
            OutputDebugStringW(L"Failed to Search VSFile.\n");
            return false;
        }

        if (!SearchFilePathW(L"SimpleTexPS.cso", PSPath))
        {
            OutputDebugStringW(L"Failed to Search PSFile.\n");
=======
        std::wstring VSPath = {};
        std::wstring PSPath = {};

        if (!SearchFilePath(L"SimpleVS.cso", VSPath))
        {
            return false;
        }

        if (!SearchFilePath(L"SimplePS.cso", PSPath))
        {
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
            return false;
        }

        // VS
        auto HR = D3DReadFileToBlob(VSPath.c_str(), VSBlob.GetAddressOf());
        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed to Load VS.\n");
            return false;
        }

        // PS
        HR = D3DReadFileToBlob(PSPath.c_str(), PSBlob.GetAddressOf());
        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed to Load PS.\n");
            return false;
        }

        // Assign PiplineState
        D3D12_GRAPHICS_PIPELINE_STATE_DESC PipelineDesc = {};
<<<<<<< HEAD
        PipelineDesc.InputLayout = /*{ ElementDescs, _countof(ElementDescs) }*/ MeshVertex::InputLayout;
=======
        PipelineDesc.InputLayout = MeshVertex::InputLayout;
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
        PipelineDesc.pRootSignature = RootSignature.Get();
        PipelineDesc.VS = {VSBlob->GetBufferPointer(), VSBlob->GetBufferSize()};
        PipelineDesc.PS = {PSBlob->GetBufferPointer(), PSBlob->GetBufferSize()};
        PipelineDesc.RasterizerState = RSDesc;
        PipelineDesc.BlendState = BlendStateDesc;
        PipelineDesc.DepthStencilState = DSSDesc;
        PipelineDesc.SampleMask = UINT_MAX;
        PipelineDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
        PipelineDesc.NumRenderTargets = 1;
        PipelineDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
        PipelineDesc.DSVFormat = DXGI_FORMAT_D32_FLOAT;
        PipelineDesc.SampleDesc.Count = 1;
        PipelineDesc.SampleDesc.Quality = 0;

        // Generate PSO
        HR = D3D12Device->CreateGraphicsPipelineState(
            &PipelineDesc,
            IID_PPV_ARGS(PSO.GetAddressOf())
        );

        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed to generate PSO.\n");
            return false;
        }
    }

<<<<<<< HEAD
    // テクスチャ生成
    {
        // ファイルパス検索
        std::wstring texturePath;
        if (!SearchFilePathW(L"res/teapot/default.dds", texturePath))
        {
            OutputDebugStringW(L"Failed to Load Texture.\n");
            return false;
        }

        DirectX::ResourceUploadBatch Batch(D3D12Device.Get());
        Batch.Begin();

        // リソース生成
        auto HR = DirectX::CreateDDSTextureFromFile(
            D3D12Device.Get(),
            Batch, 
            texturePath.c_str(),
            ColorTexture.pResource.GetAddressOf(),
            true
        );

        auto Future = Batch.End(CommandQueue.Get());
        Future.wait();

        auto incrementSize = D3D12Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

        auto HandleCPU = HeapCBV->GetCPUDescriptorHandleForHeapStart();
        auto HandleGPU = HeapCBV->GetGPUDescriptorHandleForHeapStart();

        ColorTexture.HandleCPU = HandleCPU;
        ColorTexture.HandleGPU = HandleGPU;

        auto TextureDesc = ColorTexture.pResource->GetDesc();

        // リソースビューの設定
=======
    // Generate Texture
    {
        // Search Texture
        std::wstring TexturePath = {};
        if (!SearchFilePath(L"res/SampleTexture.dds", TexturePath))
        {
            OutputDebugStringW(L"Not find texture.\n");
            return false;
        }

        DirectX::ResourceUploadBatch Bach(D3D12Device.Get());
        Bach.Begin();

        // Create Resource
        auto HR = DirectX::CreateDDSTextureFromFile(
            D3D12Device.Get(),
            Bach,
            TexturePath.c_str(),
            Texture_.pResource.GetAddressOf(),
            true
        );

        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed to generate texture.\n");
            return false;
        }

        auto Future = Bach.End(CommandQueue.Get());
        Future.wait();

        auto IncrementSize = D3D12Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
        auto HandleCPU = HeapCBV_SRV_UAV->GetCPUDescriptorHandleForHeapStart();
        auto HandleGPU = HeapCBV_SRV_UAV->GetGPUDescriptorHandleForHeapStart();

        // perhaps... affected previous increment ?
        HandleCPU.ptr += IncrementSize * 2;
        HandleGPU.ptr += IncrementSize * 2;

        Texture_.HandleCPU = HandleCPU;
        Texture_.HandleGPU = HandleGPU;

        // Get TextureConf
        auto TextureDesc = Texture_.pResource->GetDesc();
        
        // Assign SRV of Conf
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
        D3D12_SHADER_RESOURCE_VIEW_DESC ViewDesc = {};
        ViewDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
        ViewDesc.Format = TextureDesc.Format;
        ViewDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
        ViewDesc.Texture2D.MostDetailedMip = 0;
        ViewDesc.Texture2D.MipLevels = TextureDesc.MipLevels;
        ViewDesc.Texture2D.PlaneSlice = 0;
        ViewDesc.Texture2D.ResourceMinLODClamp = 0.0f;

<<<<<<< HEAD
        // 生成
        D3D12Device->CreateShaderResourceView(
            ColorTexture.pResource.Get(), &ViewDesc, HandleCPU);

    }


=======
        // Generate SRV
        D3D12Device->CreateShaderResourceView(Texture_.pResource.Get(), &ViewDesc, HandleCPU);
    }

>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
    // Create ViewPort
    {
        Viewport.TopLeftX = 0;
        Viewport.TopLeftY = 0;
        Viewport.Width = static_cast<float>(Width);
        Viewport.Height = static_cast<float>(Height);
        Viewport.MinDepth = 0.0f;
        Viewport.MaxDepth = 1.0f;

        Scissor.left = 0;
        Scissor.right = Width;
        Scissor.top = 0;
        Scissor.bottom = Height;
    }

    CommandList->Close();

    return true;
}

void App::OnTerminate()
{
    // Remove CB
    for (auto i = 0; i < FrameCount; ++i)
    {
        if (ConstantBuffer[i].Get())
        {
            ConstantBuffer[i]->Unmap(0, nullptr);
            memset(&CBView[i], 0, sizeof(CBView[i]));
        }
        ConstantBuffer[i].Reset();
    }
    
   
    IndexBuffer.Reset();
    VertexBuffer.Reset();
   
    HeapCBV.Reset();

    VBView.BufferLocation = 0;
    VBView.SizeInBytes = 0;
    VBView.StrideInBytes = 0;

    IBView.BufferLocation = 0;
    IBView.SizeInBytes = 0;
    IBView.Format = DXGI_FORMAT_UNKNOWN;

    DepthStencilBuffer.Reset();
    HeapDSV.Reset();

    RootSignature.Reset();
    PSO.Reset();
<<<<<<< HEAD

    ColorTexture.pResource.Reset();
    ColorTexture.HandleCPU.ptr = 0;
    ColorTexture.HandleGPU.ptr = 0;


=======
    HeapCBV_SRV_UAV.Reset();

    VBView.BufferLocation = 0;
    VBView.SizeInBytes = 0;
    VBView.StrideInBytes = 0;

    IBView.BufferLocation = 0;
    IBView.Format = DXGI_FORMAT_UNKNOWN;
    IBView.SizeInBytes = 0;

    RootSignature.Reset();

    Texture_.pResource.Reset();
    Texture_.HandleCPU.ptr = 0;
    Texture_.HandleCPU.ptr = 0;
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
}

void App::Render()
{
    if (!D3D12Device)
    {
        return;
    }

    if (!CBView->Buffer)
    {
        return;
    }

    // Update
    {
        CBView[FrameIndex].Buffer->World = DirectX::XMMatrixScaling(1.1f, 1.1f, 1.1f);

        RotateAngle += 0.025f;
        CBView[FrameIndex].Buffer->World *=
            DirectX::XMMatrixRotationY(RotateAngle + DirectX::XMConvertToRadians(15.0f));

        //RotateAngle += 0.025f;
        //CBView[FrameIndex * 2 + 0].Buffer->World =
        //    DirectX::XMMatrixRotationZ(RotateAngle + DirectX::XMConvertToRadians(45.0f));
     
        //CBView[FrameIndex * 2 + 1].Buffer->World =
        //    DirectX::XMMatrixRotationY(RotateAngle) * DirectX::XMMatrixScaling(2.0f, 0.5, 1.0f);
    }

    // record commands
    CommandAllocator[FrameIndex]->Reset();
    CommandList->Reset(CommandAllocator[FrameIndex].Get(), nullptr);

    // set resourceBarrier
    D3D12_RESOURCE_BARRIER Barrier = {};
    Barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
    Barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
    Barrier.Transition.pResource = ColorBuffer[FrameIndex].Get();
    Barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT;
    Barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_RENDER_TARGET;
    Barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
    CommandList->ResourceBarrier(1, &Barrier);
    
    // set renderTarget
    // handle = GPUのポインタを指す
    CommandList->OMSetRenderTargets(1, &HandleRTV[FrameIndex], FALSE, &HandleDSV);

    // set ClearColor
    float ClearColor[] = { 0.25f, 0.25f, 0.65f, 1.0f };

    // clear RTV
    CommandList->ClearRenderTargetView(HandleRTV[FrameIndex], ClearColor, 0, nullptr);

    // ClearDSV
    CommandList->ClearDepthStencilView(HandleDSV, D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, nullptr);

    // DrawProcess 
    {
        CommandList->SetGraphicsRootSignature(RootSignature.Get());
<<<<<<< HEAD
        CommandList->SetDescriptorHeaps(1, HeapCBV.GetAddressOf());
        CommandList->SetGraphicsRootConstantBufferView(0, CBView[FrameIndex].Desc.BufferLocation);
        CommandList->SetGraphicsRootDescriptorTable(1, ColorTexture.HandleGPU);
=======
        CommandList->SetDescriptorHeaps(1, HeapCBV_SRV_UAV.GetAddressOf());
        CommandList->SetGraphicsRootConstantBufferView(0, CBView[FrameIndex * 2 + 0].Desc.BufferLocation);
        CommandList->SetGraphicsRootDescriptorTable(1, Texture_.HandleGPU);
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
        CommandList->SetPipelineState(PSO.Get());

        CommandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
        CommandList->IASetVertexBuffers(0, 1, &VBView);
        CommandList->IASetIndexBuffer(&IBView);
        CommandList->RSSetViewports(1, &Viewport);
        CommandList->RSSetScissorRects(1, &Scissor);

<<<<<<< HEAD
        //CommandList->DrawIndexedInstanced(6, 1, 0, 0, 0);
        auto Count = static_cast<uint32_t>(StaticMeshes[0].Indices.size());
        CommandList->DrawIndexedInstanced(Count, 1, 0, 0, 0);

        //CommandList->SetGraphicsRootConstantBufferView(0, CBView[FrameIndex * 2 + 0].Desc.BufferLocation);
        //CommandList->DrawIndexedInstanced(6, 1, 0, 0, 0);

        //CommandList->SetGraphicsRootConstantBufferView(0, CBView[FrameIndex * 2 + 1].Desc.BufferLocation);
=======
        auto Count = static_cast<uint32_t>(Meshes[0].Indices.size());
        CommandList->DrawIndexedInstanced(Count, 1, 0, 0, 0);

        CommandList->SetGraphicsRootConstantBufferView(0, CBView[FrameIndex * 2 + 1].Desc.BufferLocation);
>>>>>>> 72931bc661b785ca5655e9f999ac1682f4a1cf55
        //CommandList->DrawIndexedInstanced(6, 1, 0, 0, 0);
    }

    // set resourceBarrier
    Barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
    Barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
    Barrier.Transition.pResource = ColorBuffer[FrameIndex].Get();
    Barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_RENDER_TARGET;
    Barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PRESENT;
    Barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
    CommandList->ResourceBarrier(1, &Barrier);

    // end record command
    CommandList->Close();

    // execute command
    ID3D12CommandList* ppCmdList[] = { CommandList.Get() };
    CommandQueue->ExecuteCommandLists(1, ppCmdList);

    // show renderResult to display
    Present(1);
}

void App::WaitGPU()
{
    assert(CommandQueue);
    assert(Fence);
    assert(FenceEvent);

    CommandQueue->Signal(Fence.Get(), FenceCounter[FrameIndex]);
    Fence->SetEventOnCompletion(FenceCounter[FrameIndex], FenceEvent);

    WaitForSingleObjectEx(FenceEvent, INFINITE, FALSE);

    FenceCounter[FrameIndex]++;
}

void App::Present(uint32_t Interval)
{
    SwapChain->Present(Interval, 0);
    const auto CurrentValue = FenceCounter[FrameIndex];
    CommandQueue->Signal(Fence.Get(), CurrentValue);

    FrameIndex = SwapChain->GetCurrentBackBufferIndex();

    if (Fence->GetCompletedValue() < FenceCounter[FrameIndex])
    {
        Fence->SetEventOnCompletion(FenceCounter[FrameIndex], FenceEvent);
        WaitForSingleObjectEx(FenceEvent, INFINITE, FALSE);
    }

    FenceCounter[FrameIndex] = CurrentValue + 1;
}

LRESULT CALLBACK App::WndProc(HWND HWnd, UINT Msg, WPARAM Wp, LPARAM Lp)
{
    switch (Msg)
    {
        case WM_DESTROY:
        {
            PostQuitMessage(0);
            break;
        }

        default:
        {
            break;
        }
    }

    return DefWindowProc(HWnd, Msg, Wp, Lp);
}