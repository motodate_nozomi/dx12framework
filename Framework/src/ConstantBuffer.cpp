#include "ConstantBuffer.h"
#include "DescriptorPool.h"
#include <Logger.h>

ConstantBuffer::ConstantBuffer()
{

}

ConstantBuffer::~ConstantBuffer()
{
    Terminate();
}

const bool ConstantBuffer::Initialize(ID3D12Device* Device, 
    DescriptorPool* Pool, 
    size_t Size)
{
    // 値の有効性を調べる
    if (!Device)
    {
        ELOG("ConstantBuffer::Initialize >> Device が nullptr.");
        return false;
    }

    if (!Pool)
    {
        ELOG("ConstantBuffer::Initialize >> Pool が nullptr.");
        return false;
    }

    if (Size <= 0)
    {
        ELOG("ConstantBuffer::Initialize >> Size <= 0.");
        return false;
    }

    // プールの設定
    this->Pool = Pool;
    this->Pool->AddRef();

    // メモリサイズの切り上げ(アラインメントを揃える)
    size_t Align = D3D12_CONSTANT_BUFFER_DATA_PLACEMENT_ALIGNMENT;
    UINT64 SizeAligned = (Size + (Align - 1)) & ~(Align - 1);

    // ヒープコンフィグ
    D3D12_HEAP_PROPERTIES HeapProp = {};
    HeapProp.Type = D3D12_HEAP_TYPE_UPLOAD;
    HeapProp.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
    HeapProp.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
    HeapProp.CreationNodeMask = 1;
    HeapProp.VisibleNodeMask = 1;

    // リソースコンフィグ
    D3D12_RESOURCE_DESC Desc = {};
    Desc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
    Desc.Alignment = 0;
    Desc.Width = SizeAligned;
    Desc.Height = 1;
    Desc.DepthOrArraySize = 1;
    Desc.MipLevels = 1;
    Desc.Format = DXGI_FORMAT_UNKNOWN;
    Desc.SampleDesc.Count = 1;
    Desc.SampleDesc.Quality = 0;
    Desc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
    Desc.Flags = D3D12_RESOURCE_FLAG_NONE;

    // リソース生成
    auto HR = Device->CreateCommittedResource(
        &HeapProp,
        D3D12_HEAP_FLAG_NONE,
        &Desc,
        D3D12_RESOURCE_STATE_GENERIC_READ,
        nullptr,
        IID_PPV_ARGS(Resource.GetAddressOf()));

    if (FAILED(HR))
    {
        ELOG("ConstantBuffer::Initialize >> リソースの生成に失敗.");
        return false;
    }

    // メモリマッピング
    HR = Resource->Map(0, nullptr, &MappedPtr);
    if (FAILED(HR))
    {
        ELOG("ConstantBuffer::Initialize >> メモリマッピングに失敗.");
        return false;
    }

    ViewDesc.BufferLocation = Resource->GetGPUVirtualAddress();
    ViewDesc.SizeInBytes = UINT(SizeAligned);
    Handle = Pool->AllocHandle();

    if (!Handle)
    {
        ELOG("ConstantBuffer::Initialize >> ハンドルの取得に失敗.");
        return false;
    }

    Device->CreateConstantBufferView(&ViewDesc, Handle->CPUHandle);

    return true;
}

void ConstantBuffer::Terminate()
{
    if (Resource)
    {
        Resource->Unmap(0, nullptr);
        Resource.Reset();
    }

    if (Pool)
    {
        Pool->FreeHandle(Handle);
        Handle = nullptr;

        Pool->Release();
        Pool = nullptr;
    }

    MappedPtr = nullptr;
}

const D3D12_GPU_VIRTUAL_ADDRESS ConstantBuffer::GetVirtualAdress() const
{
    return ViewDesc.BufferLocation;
}

const D3D12_CPU_DESCRIPTOR_HANDLE ConstantBuffer::GetCPUHandle() const
{
    if (!Handle)
    {
        ELOG("ConstantBuffer::GetCPUHandle >> ハンドルの取得に失敗.");
        return D3D12_CPU_DESCRIPTOR_HANDLE();
    }

    return Handle->CPUHandle;
}

const D3D12_GPU_DESCRIPTOR_HANDLE ConstantBuffer::GetGPUHandle() const
{
    if (!Handle)
    {
        ELOG("ConstantBuffer::GetGPUHandle >> ハンドルの取得に失敗.");
        return D3D12_GPU_DESCRIPTOR_HANDLE();
    }

    return Handle->GPUHandle;
}

void* const ConstantBuffer::GetMappedPtr() const
{
    return MappedPtr;
}
