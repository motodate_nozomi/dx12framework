#include "DepthTarget.h"
#include <DescriptorPool.h>
#include <Logger.h>

DepthTarget::DepthTarget()
{

}

DepthTarget::~DepthTarget()
{
    Termiante();
}

const bool DepthTarget::Initialize(
    ID3D12Device* const Device,
    DescriptorPool* const DSVPool,
    uint32_t Width,
    uint32_t Height,
    DXGI_FORMAT Format)
{
    // 引数の有効性をチェック
    if (!Device)
    {
        ELOG("DepthTarget::Initialize >> Device is nullptr.");
        return false;
    }

    if (!DSVPool)
    {
        ELOG("DepthTarget::Initialize >> DSVPool is nullptr.");
        return false;
    }

    if (Width <= 0 || Height <= 0)
    {
        ELOG("DepthTarget::Initialize >> Width <= 0 || Height <= 0.");
        return false;
    }

    // プール設定
    this->DSVPool = DSVPool;
    this->DSVPool->AddRef();
    DSVHandle = DSVPool->AllocHandle();
    if (!DSVHandle)
    {
        ELOG("DepthTarget::Initialize >> Failed to set DSVPool.");
        return false;
    }

    // ヒープ設定
    D3D12_HEAP_PROPERTIES HeapProp = {};
    HeapProp.Type = D3D12_HEAP_TYPE_DEFAULT;
    HeapProp.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
    HeapProp.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
    HeapProp.CreationNodeMask = 1;
    HeapProp.VisibleNodeMask = 1;

    // リソース設定
    D3D12_RESOURCE_DESC ResrcDesc = {};
    ResrcDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
    ResrcDesc.Alignment = 0;
    ResrcDesc.Width = UINT64(Width);
    ResrcDesc.Height = Height;
    ResrcDesc.DepthOrArraySize = 1;
    ResrcDesc.MipLevels = 1;
    ResrcDesc.Format = Format;
    ResrcDesc.SampleDesc.Count = 1;
    ResrcDesc.SampleDesc.Quality = 0;
    ResrcDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
    ResrcDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

    D3D12_CLEAR_VALUE ClearValue = {};
    ClearValue.Format = Format;
    ClearValue.DepthStencil.Depth = 1.0f;
    ClearValue.DepthStencil.Stencil = 0;

    auto HR = Device->CreateCommittedResource(
        &HeapProp,
        D3D12_HEAP_FLAG_NONE,
        &ResrcDesc,
        D3D12_RESOURCE_STATE_DEPTH_WRITE,
        &ClearValue,
        IID_PPV_ARGS(Resource.GetAddressOf()));

    if (FAILED(HR))
    {
        ELOG("DepthTarget::Initialize >> Failed to create Resource.");
        return false;
    }

    // ビュー設定(リソースをどのように扱うか)
    ViewDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
    ViewDesc.Format = Format;
    ViewDesc.Texture2D.MipSlice = 0;
    ViewDesc.Flags = D3D12_DSV_FLAG_NONE;

    Device->CreateDepthStencilView(
        Resource.Get(),
        &ViewDesc,
        DSVHandle->CPUHandle);

    return true;

}

void DepthTarget::Termiante()
{
    if (Resource)
    {
        Resource.Reset();
        Resource = nullptr;
    }

    if (DSVPool && DSVHandle)
    {
        DSVPool->FreeHandle(DSVHandle);
        DSVHandle = nullptr;

        DSVPool->Release();
        DSVPool = nullptr;
    }
}

DescriptorHandle* const DepthTarget::GetDSVHandle() const
{
    return DSVHandle;
}

ID3D12Resource* const DepthTarget::GetResource() const
{
    if (!Resource)
    {
        return nullptr;
    }

    return Resource.Get();
}

const D3D12_RESOURCE_DESC DepthTarget::GetDesc() const
{
    if (!Resource)
    {
        return D3D12_RESOURCE_DESC();
    }

    return Resource->GetDesc();
}

const D3D12_DEPTH_STENCIL_VIEW_DESC DepthTarget::GetViewDesc() const
{
    return ViewDesc;
}
