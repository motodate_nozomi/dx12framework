#include "Logger.h"

#include <cstdio>
#include <cstdarg>
#include <Windows.h>

void OutputLog(const char* LogFormat, ...)
{
    char Msg[2048];
    memset(Msg, '\0', sizeof(Msg));
    va_list Arg;

    va_start(Arg, LogFormat);
    vsprintf_s(Msg, LogFormat, Arg);
    va_end(Arg);

    printf_s("%s", Msg);
    OutputDebugStringA(Msg);
}