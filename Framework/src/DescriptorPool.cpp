#include "DescriptorPool.h"


DescriptorPool::DescriptorPool()
{

}

DescriptorPool::~DescriptorPool()
{
    HandlePool.Terminate();
    Heap.Reset();
    Heap = nullptr;
    DescriptorSize = 0;
}

bool DescriptorPool::Create(ID3D12Device* Device, 
    const D3D12_DESCRIPTOR_HEAP_DESC* Desc, 
    DescriptorPool** PoolPtr)
{
    // 無効アドレスチェック
    if (!Device)
    {
        OutputDebugStringW(L"DescriptorPool::Create >> Device が nullptr. \n");
        return false;
    }

    if (!Desc)
    {
        OutputDebugStringW(L"DescriptorPool::Create >> Desc が nullptr. \n");
        return false;
    }

    if (!PoolPtr)
    {
        OutputDebugStringW(L"DescriptorPool::Create >> PoolPtr が nullptr. \n");
        return false;
    }

    // インスタンス生成
    DescriptorPool* Instance = new (std::nothrow) DescriptorPool();
    if (!Instance)
    {
        OutputDebugStringW(L"DescriptorPool::Create >> インスタンスの生成に失敗. \n");
        return false;
    }

    // ヒープ生成
    auto HR = Device->CreateDescriptorHeap(Desc, IID_PPV_ARGS(Instance->Heap.GetAddressOf()));
    if (FAILED(HR))
    {
        OutputDebugStringW(L"DescriptorPool::Create >> ヒープの生成に失敗. \n");
        Instance->Release();
        return false;
    }

    // プール初期化
    if (!(Instance->HandlePool.Initialize(Desc->NumDescriptors)))
    {
        OutputDebugStringW(L"DescriptorPool::Create >> プール初期化に失敗. \n");
        Instance->Release();
        return false;
    }

    // ディスクリプタのサイズを取得
    Instance->DescriptorSize = Device->GetDescriptorHandleIncrementSize(Desc->Type);

    *PoolPtr = Instance;

    return true;
}

void DescriptorPool::AddRef()
{
    ++RefCount;
}

void DescriptorPool::Release()
{
    --RefCount;
    if (RefCount <= 0)
    {
        OutputDebugStringW(L"DescriptorPool::Release >> ディスクリプタプールの参照がすべて切れたため、破棄. \n");
        delete this;
    }
}

const uint32_t DescriptorPool::GetRefCount() const
{
    return RefCount;
}

DescriptorHandle* DescriptorPool::AllocHandle()
{
    // 初期化関数を定義
    auto InitializeFunc = [&](uint32_t Index, DescriptorHandle* HandlePtr)
    {
        if (!HandlePtr)
        {
            return;
        }

        auto CPUHandle = Heap->GetCPUDescriptorHandleForHeapStart();
        CPUHandle.ptr += DescriptorSize * Index;

        auto GPUHandle = Heap->GetGPUDescriptorHandleForHeapStart();
        GPUHandle.ptr += DescriptorSize * Index;

        HandlePtr->CPUHandle = CPUHandle;
        HandlePtr->GPUHandle = GPUHandle;
    };

    return HandlePool.Alloc(InitializeFunc);
}

void DescriptorPool::FreeHandle(DescriptorHandle*& HandlePtr)
{
    if(!HandlePtr)
    {
        return;
    }

    HandlePool.Free(HandlePtr);
    HandlePtr = nullptr;
}

uint32_t DescriptorPool::GetAvailableHandleCount() const
{
    return HandlePool.GetAvailableCount();
}

uint32_t DescriptorPool::GetAllocatedHandleCount() const
{
    return HandlePool.GetUsedCount();
}

uint32_t DescriptorPool::GetHandleCount() const
{
    return HandlePool.GetCapacity();
}

ID3D12DescriptorHeap* const DescriptorPool::GetHeap() const
{
    if (!Heap)
    {
        OutputDebugStringW(L"DescriptorPool::GetHeap >> ヒープが存在しません. \n");
        return nullptr;
    }

    return Heap.Get();
}
