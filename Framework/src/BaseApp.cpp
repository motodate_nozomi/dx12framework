#include "BaseApp.h"
#include <Logger.h>


namespace
{
    const auto ClassName = TEXT("BaseApp");
}

BaseApp::BaseApp(const uint32_t Width, const uint32_t Height)
: HInstance(nullptr)
, HWnd(nullptr)
, Width(Width)
, Height(Height)
, FrameIndex(0)
{}

BaseApp::~BaseApp()
{}

void BaseApp::Run()
{
    if (InitializeApp())
    {
        MainLoop();
    }

    DLOG("アプリケーションを終了します.");
    TermianteApp();
}

void BaseApp::Present(const uint32_t Interval)
{
    if (SwapChain)
    {
        ELOG("BaseApp::Present >> SwapChain is nullptr.");
        return;
    }

    SwapChain->Present(Interval, 0);

    GPUFence.Sync(GFXCommandQueue.Get());

    FrameIndex = SwapChain->GetCurrentBackBufferIndex();
}

const bool BaseApp::InitializeApp()
{
    if (!InitializeWindow())
    {
        ELOG("ウィンドウの初期化に失敗.\n");
        return false;
    }

    if (!InitializeD3D())
    {
        ELOG("グラフィックスシステムの初期化に失敗.\n");
        return false;
    }

    return true;
}

void BaseApp::TermianteApp()
{
    TermianteWindow();

    TerminateD3D();
}

const bool BaseApp::InitializeWindow()
{
    // インスタンスハンドルの取得
    auto HInst = GetModuleHandle(nullptr);
    if (!HInst)
    {
        ELOG("Not find HInstance.\n");
        return false;
    }

    // ウィンドウ設定
    WNDCLASSEX WC = {};
    WC.cbSize = sizeof(WNDCLASSEX);
    WC.style = CS_HREDRAW | CS_VREDRAW;
    WC.lpfnWndProc = WndProc;
    WC.hIcon = LoadIcon(HInst, IDI_APPLICATION);
    WC.hCursor = LoadCursor(HInst, IDC_ARROW);
    WC.hbrBackground = GetSysColorBrush(COLOR_BACKGROUND);
    WC.lpszMenuName = nullptr;
    WC.lpszClassName = ClassName;
    WC.hIconSm = LoadIcon(HInst, IDI_APPLICATION);

    // ウィンドウ登録
    if (!RegisterClassEx(&WC))
    {
        ELOG("Failed Register Window.\n");
        return false;
    }
    this->HInstance = HInst;

    // ウィンドウサイズ設定
    RECT RC = {};
    RC.right = static_cast<LONG>(Width);
    RC.bottom = static_cast<LONG>(Height);
    auto Style = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU;
    AdjustWindowRect(&RC, Style, FALSE);

    // ウィンドウ生成
    HWnd = CreateWindowEx(
        0,
        ClassName,
        TEXT("Framework"),
        Style,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        RC.right - RC.left,
        RC.bottom - RC.top,
        nullptr,
        nullptr,
        HInstance,
        nullptr);

    if (!HWnd)
    {
        ELOG("ウィンドウの生成に失敗.\n");
        return false;
    }

    return true;
}

void BaseApp::TermianteWindow()
{
    if (HInstance)
    {
        UnregisterClass(ClassName, HInstance);
    }

    HInstance = nullptr;
    HWnd = nullptr;
}

const bool BaseApp::InitializeD3D()
{
    // デバッグ機能の有効化
    #if defined(DEBUG) || defined(_DEBUG)
    {
        ComPtr<ID3D12Debug> Debug = nullptr;
        auto HR = D3D12GetDebugInterface(IID_PPV_ARGS(Debug.GetAddressOf()));
        if (SUCCEEDED(HR))
        {
            Debug->EnableDebugLayer();
        }
    }
    #endif

    // デバイスの生成
    auto HR = D3D12CreateDevice(
        nullptr,
        D3D_FEATURE_LEVEL_11_0,
        IID_PPV_ARGS(D3D12Device.GetAddressOf())
    );

    // コマンドキュー
    {
        D3D12_COMMAND_QUEUE_DESC Desc = {};
        Desc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
        Desc.Priority = D3D12_COMMAND_QUEUE_PRIORITY_NORMAL;
        Desc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
        Desc.NodeMask = 0;

        HR = D3D12Device->CreateCommandQueue(&Desc, IID_PPV_ARGS(GFXCommandQueue.GetAddressOf()));
    }

    // スワップチェインの作成
    {
        ComPtr<IDXGIFactory4> Factory = nullptr;
        HR = CreateDXGIFactory1(IID_PPV_ARGS(Factory.GetAddressOf()));
        if (FAILED(HR))
        {
            ELOG("Failed create dxgiFactory for swapChain.");
            return false;
        }

        DXGI_SWAP_CHAIN_DESC Desc = {};
        Desc.BufferDesc.Width = this->Width;
        Desc.BufferDesc.Height = this->Height;
        Desc.BufferDesc.RefreshRate.Numerator = 60;
        Desc.BufferDesc.RefreshRate.Denominator = 1;
        Desc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
        Desc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
        Desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
        Desc.SampleDesc.Count = 1;
        Desc.SampleDesc.Quality = 0;
        Desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
        Desc.BufferCount = FrameCount;
        Desc.OutputWindow = HWnd;
        Desc.Windowed = TRUE;
        Desc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
        Desc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

        ComPtr<IDXGISwapChain> TempSwapChain = nullptr;
        HR = Factory->CreateSwapChain(GFXCommandQueue.Get(), &Desc, TempSwapChain.GetAddressOf());
        if (FAILED(HR))
        {
            ELOG("Failed create TempswapChain.\n");
            return false;
        }

        HR = TempSwapChain.As(&SwapChain);
        if (FAILED(HR))
        {
            ELOG("Failed create swapChain3.\n");
            return false;
        }

        // get buckBufferNum
        FrameIndex = SwapChain->GetCurrentBackBufferIndex();

        Factory.Reset();
        TempSwapChain.Reset();
    }

    // ディスクリプタプールの作成
    {
        D3D12_DESCRIPTOR_HEAP_DESC HeapDesc = {};

        // リソース
        HeapDesc.NodeMask = 1;
        HeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
        HeapDesc.NumDescriptors = 512;
        HeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
        if (!DescriptorPool::Create(D3D12Device.Get(), &HeapDesc, &Pool[POOL_TYPE_RES]))
        {
            ELOG("POOL_TYPE_RESの生成失敗.\n");
            return false;
        }

        // サンプラー
        HeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER;
        HeapDesc.NumDescriptors = 256;
        HeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
        if (!DescriptorPool::Create(D3D12Device.Get(), &HeapDesc, &Pool[POOL_TYPE_SMP]))
        {
            ELOG("POOL_TYPE_SMPの生成失敗.\n");
            return false;
        }

        // レンダーターゲット
        HeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
        HeapDesc.NumDescriptors = 512;
        HeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
        if (!DescriptorPool::Create(D3D12Device.Get(), &HeapDesc, &Pool[POOL_TYPE_RTV]))
        {
            ELOG("POOL_TYPE_RTVの生成失敗.\n");
            return false;
        }

        // デプスステンシル
        HeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
        HeapDesc.NumDescriptors = 512;
        HeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
        if (!DescriptorPool::Create(D3D12Device.Get(), &HeapDesc, &Pool[POOL_TYPE_DSV]))
        {
            ELOG("POOL_TYPE_DSVの生成失敗.\n");
            return false;
        }
    }

    // コマンドリストの生成
    if (!GFXCommandList.Initialize(D3D12Device.Get(),
        D3D12_COMMAND_LIST_TYPE_DIRECT, FrameCount))
    {
        ELOG("コマンドリストの初期化に失敗.\n");
        return false;
    }

    // レンダーターゲットビューの生成
    {
        for (auto FrameNum = 0u; FrameNum < FrameCount; ++FrameNum)
        {
            if (!ColorTargetList[FrameNum].InitializeFromBackBuffer(
                D3D12Device.Get(),
                Pool[POOL_TYPE_RTV],
                FrameNum,
                SwapChain.Get()))
            {
                ELOG("RTVの生成に失敗.\n");
                return false;
            }
        }
    }

    // デプスステンシルバッファの生成
    {
        if (!DepthStencilTarget.Initialize(
            D3D12Device.Get(),
            Pool[POOL_TYPE_DSV],
            Width,
            Height,
            DXGI_FORMAT_R32_FLOAT))
        {
            ELOG("デプスステンシルバッファの生成の生成に失敗.\n");
            return false;
        }
    }
    
    // フェンスの生成
    if (!GPUFence.Initialize(D3D12Device.Get()))
    {
        ELOG("フェンスの生成の生成に失敗.\n");
        return false;
    }

    // ビューポートの設定
    {
        Viewport.TopLeftX = 0.0f;
        Viewport.TopLeftY = 0.0f;
        Viewport.Width = float(Width);
        Viewport.Height = float(Height);
        Viewport.MinDepth = 0.0f;
        Viewport.MaxDepth = 1.0f;
    }

    // シザー矩形の設定
    {
        Scissor.left = 0;
        Scissor.right = Width;
        Scissor.top = 0;
        Scissor.bottom = Height;
    }

    return true;
}

void BaseApp::TerminateD3D()
{
    // GPU処理完了まで待ち、フェンスを破棄
    GPUFence.Sync(GFXCommandQueue.Get());
    GPUFence.Terminate();

    // RTの破棄
    for (auto RTIndex = 0u; RTIndex < FrameCount; ++RTIndex)
    {
        ColorTargetList[RTIndex].Terminate();
    }

    // 深度ステンシルビューの破棄
    DepthStencilTarget.Termiante();

    // コマンドリストの破棄
    GFXCommandList.Terminate();

    // プールの破棄
    for (auto PoolIndex = 0u; PoolIndex < POOL_COUNT; ++PoolIndex)
    {
        if (Pool[PoolIndex])
        {
            Pool[PoolIndex]->Release();
            Pool[PoolIndex] = nullptr;
        }
    }

    // スワップチェイン
    SwapChain.Reset();

    // コマンドキュー
    GFXCommandQueue.Reset();

    // デバイス
    D3D12Device.Reset();
}

void BaseApp::MainLoop()
{
    MSG Msg = {};
    while (WM_QUIT != Msg.message)
    {
        if (PeekMessage(&Msg, nullptr, 0, 0, PM_REMOVE) == TRUE)
        {
            TranslateMessage(&Msg);
            DispatchMessage(&Msg);
        }
        else
        {
            OnRender();
        }
    }
}

LRESULT CALLBACK BaseApp::WndProc(HWND HWnd, UINT Msg, WPARAM Wp, LPARAM Lp)
{
    auto Instance = reinterpret_cast<BaseApp*>(GetWindowLongPtr(HWnd, GWLP_USERDATA));

    switch (Msg)
    {
        case WM_CREATE:
        {
            auto CreateStruct = reinterpret_cast<LPCREATESTRUCT>(Lp);
            SetWindowLongPtr(HWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(CreateStruct->lpCreateParams));
            break;
        }

        case WM_DESTROY:
        {
            PostQuitMessage(0);
            break;
        }

        default:
        break;
    }

    if (Instance)
    {
        Instance->OnMsgProc(HWnd, Msg, Wp, Lp);
    }

    return DefWindowProc(HWnd, Msg, Wp, Lp);
}
