#include "ColorTarget.h"
#include <Logger.h>
#include <DescriptorPool.h>

ColorTarget::ColorTarget()
{

}

ColorTarget::~ColorTarget()
{
    Terminate();
}

const bool ColorTarget::Initialize(
    ID3D12Device* Device,
    DescriptorPool* Pool,
    uint32_t Width, 
    uint32_t Height,
    DXGI_FORMAT Format)
{
    if (!Device)
    {
        ELOG("ColorTarget::Initialize >> Device が nullptr.");
        return false;
    }

    if (!Pool)
    {
        ELOG("ColorTarget::Initialize >> Pool が nullptr.");
        return false;
    }

    if (Width <= 0 || Height <= 0)
    {
        ELOG("ColorTarget::Initialize >> Width <= 0 || Height <= 0.");
        return false;
    }

    // プール設定
    RTVPool = Pool;
    RTVPool->AddRef();

    // ハンドル設定
    RTVHandle = RTVPool->AllocHandle();
    if (!RTVHandle)
    {
        ELOG("ColorTarget::Initialize >> RTVHandleがnullptr.");
        return false;
    }
    
    // ヒープ設定
    D3D12_HEAP_PROPERTIES HeapProp = {};
    HeapProp.Type = D3D12_HEAP_TYPE_DEFAULT;
    HeapProp.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
    HeapProp.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
    HeapProp.CreationNodeMask = 1;
    HeapProp.VisibleNodeMask = 1;

    // リソース設定
    D3D12_RESOURCE_DESC ResrcDesc = {};
    ResrcDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
    ResrcDesc.Alignment = 0;
    ResrcDesc.Width = UINT64(Width);
    ResrcDesc.Height = Height;
    ResrcDesc.DepthOrArraySize = 1;
    ResrcDesc.MipLevels = 1;
    ResrcDesc.Format = Format;
    ResrcDesc.SampleDesc.Count = 1;
    ResrcDesc.SampleDesc.Quality = 0;
    ResrcDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
    ResrcDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET;

    D3D12_CLEAR_VALUE ClearValue = {};
    ClearValue.Format = Format;
    ClearValue.Color[0] = 1.0f;
    ClearValue.Color[1] = 1.0f;
    ClearValue.Color[2] = 1.0f;
    ClearValue.Color[3] = 1.0f;

    // リソース生成
    auto HR = Device->CreateCommittedResource(
        &HeapProp,
        D3D12_HEAP_FLAG_NONE,
        &ResrcDesc,
        D3D12_RESOURCE_STATE_RENDER_TARGET,
        &ClearValue,
        IID_PPV_ARGS(Resource.GetAddressOf()));

    if (FAILED(HR))
    {
        ELOG("ColorTarget::Initialize >> リソースの生成に失敗.");
        return false;
    }

    // ビュー生成
    ViewDesc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE2D;
    ViewDesc.Format = Format;
    ViewDesc.Texture2D.MipSlice = 0;
    ViewDesc.Texture2D.PlaneSlice = 0;

    Device->CreateRenderTargetView(
        Resource.Get(),
        &ViewDesc,
        RTVHandle->CPUHandle);

    return true;
}

const bool ColorTarget::InitializeFromBackBuffer(
    ID3D12Device* const Device,
    DescriptorPool* const Pool,
    uint32_t BackBufferIndex,
    IDXGISwapChain* const SwapChain)
{
    if (!Device)
    {
        ELOG("ColorTarget::InitializeFromBackBuffer >> Device is nullptr.");
        return false;
    }

    if (!Pool)
    {
        ELOG("ColorTarget::InitializeFromBackBuffer >> Pool is nullptr.");
        return false;
    }

    if (!SwapChain)
    {
        ELOG("ColorTarget::InitializeFromBackBuffer >> SwapChain is nullptr.");
        return false;
    }

    // プール設定
    RTVPool = Pool;
    RTVPool->AddRef();

    // ハンドル設定
    RTVHandle = RTVPool->AllocHandle();
    if (!RTVHandle)
    {
        ELOG("ColorTarget::InitializeFromBackBuffer >> RTVHandleがnullptr.");
        return false;
    }

    // バックバッファー取得
    auto HR = SwapChain->GetBuffer(BackBufferIndex, IID_PPV_ARGS(Resource.GetAddressOf()));
    if (FAILED(HR))
    {
        ELOG("ColorTarget::InitializeFromBackBuffer >> Failed get to SwapChain.");
        return false;
    }

    // ビュー生成
    DXGI_SWAP_CHAIN_DESC Desc = {};
    SwapChain->GetDesc(&Desc);
    
    ViewDesc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE2D;
    ViewDesc.Format = Desc.BufferDesc.Format;
    ViewDesc.Texture2D.MipSlice = 0;
    ViewDesc.Texture2D.PlaneSlice = 0;

    Device->CreateRenderTargetView(
        Resource.Get(),
        &ViewDesc,
        RTVHandle->CPUHandle);

    return true;
}

void ColorTarget::Terminate()
{
    if (Resource)
    {
        Resource.Reset();
        Resource = nullptr;
    }
  
    if (RTVPool && RTVHandle)
    {
        RTVPool->FreeHandle(RTVHandle);
        RTVHandle = nullptr;

        RTVPool->Release();
        RTVPool = nullptr;
    }
}

DescriptorHandle* const ColorTarget::GetRTVHandle() const
{
    return RTVHandle;
}

ID3D12Resource* const ColorTarget::GetResource() const
{
    if (!Resource)
    {
        return nullptr;
    }

    return Resource.Get();
}

const D3D12_RESOURCE_DESC ColorTarget::GetResourceDesc() const
{
    if (!Resource)
    {
        return D3D12_RESOURCE_DESC();
    }

    return Resource->GetDesc();
}

const D3D12_RENDER_TARGET_VIEW_DESC ColorTarget::GetRTVDesc() const
{
    return ViewDesc;
}
