#include "ResMesh.h"
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <codecvt>
#include <cassert>

// 定数データ
#define FMT_FLOAT3 DXGI_FORMAT_R32G32B32_FLOAT
#define FMT_FLOAT2 DXGI_FORMAT_R32G32_FLOAT
#define APPEND D3D12_APPEND_ALIGNED_ELEMENT
#define IL_VERTEX D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA

const D3D12_INPUT_ELEMENT_DESC MeshVertex::InputElements[] = 
{
    { "POSITION", 0, FMT_FLOAT3, 0, APPEND, IL_VERTEX, 0 },
    { "NORMAL", 0, FMT_FLOAT3, 0, APPEND, IL_VERTEX, 0 },
    { "TEXCOORD", 0, FMT_FLOAT2, 0, APPEND, IL_VERTEX, 0 },
    { "TANGENT", 0, FMT_FLOAT3, 0, APPEND, IL_VERTEX, 0 }
};

const D3D12_INPUT_LAYOUT_DESC MeshVertex::InputLayout = 
{
    MeshVertex::InputElements,
    InputElementCount
};

#undef FMT_FLOAT3
#undef FMT_FLOAT2
#undef APPEND
#undef IL_VERTEX

// 便利機能
namespace
{
    // UTF8に変換
    std::string ToUTF8(const std::wstring& Value)
    {
        auto Length = WideCharToMultiByte(CP_UTF8, 0U, Value.data(), -1, nullptr, 0, nullptr, nullptr);
        auto Buffer = new char[Length];
        WideCharToMultiByte(CP_UTF8, 0U, Value.data(), -1, Buffer, Length, nullptr, nullptr);

        std::string ResultStr(Buffer);
        delete[] Buffer;
        Buffer = nullptr;

        return ResultStr;
    }
}

// メッシュローダ
class MeshLoader
{
public:
    MeshLoader();
    ~MeshLoader();

    const bool Load(
        const wchar_t* Filename,
        std::vector<ResMesh>& Meshes,
        std::vector<ResMaterial>& Materials
    );

private:
    void ParseMesh(ResMesh& OutMesh, const aiMesh* pSrcMesh);
    void ParseMaterial(ResMaterial& OutMaterial, const aiMaterial* pSrcMaterial);
};

MeshLoader::MeshLoader()
{}

MeshLoader::~MeshLoader()
{}

const bool MeshLoader::Load(const wchar_t* Filename, std::vector<ResMesh>&Meshes, std::vector<ResMaterial>&Materials)
{
    if (!Filename)
    {
        return false;
    }

    auto Filepath = ToUTF8(Filename);

    Assimp::Importer Importer = {};
    int Flag = 0;
    Flag |= aiProcess_Triangulate;
    //Flag |= aiProcess_PreTransformVertices;
    Flag |= aiProcess_CalcTangentSpace;
    Flag |= aiProcess_GenSmoothNormals;
    Flag |= aiProcess_GenUVCoords;
    Flag |= aiProcess_RemoveRedundantMaterials;
    Flag |= aiProcess_OptimizeMeshes;

    // ファイル読み込み
    auto pScene = Importer.ReadFile(Filepath, Flag);
    if (!pScene)
    {
        OutputDebugStringW(L"Assimp: シーンが読み込めません\n");
        return false;
    }

    // メッシュデータ生成
    Meshes.clear();
    Meshes.resize(pScene->mNumMeshes);
    for (size_t MeshCounter = 0; MeshCounter < Meshes.size(); ++MeshCounter)
    {
        const auto SrcMesh = pScene->mMeshes[MeshCounter];
        ParseMesh(Meshes[MeshCounter], SrcMesh);
    }

    // マテリアルデータ生成(メッシュがマテリアルIDで参照)
    Materials.clear();
    Materials.resize(pScene->mNumMaterials);
    for (size_t MaterialCounter = 0; MaterialCounter < Materials.size(); ++MaterialCounter)
    {
        const auto SrcMaterial = pScene->mMaterials[MaterialCounter];
        ParseMaterial(Materials[MaterialCounter], SrcMaterial);
    }

    pScene = nullptr;

    return true;
}

// メッシュデータ解析
void MeshLoader::ParseMesh(ResMesh& OutMesh, const aiMesh* pSrcMesh)
{
    if(!pSrcMesh)
    {
        OutputDebugStringW(L"ソースメッシュが存在しません\n");
        return;
    }

    // マテリアルIDを取得
    OutMesh.MaterialID = pSrcMesh->mMaterialIndex;

    aiVector3D ZeroVector(0.0f, 0.0f, 0.0f);

    // 頂点データ
    OutMesh.Vertices.resize(pSrcMesh->mNumVertices);
    for (auto VertexCounter = 0U; VertexCounter < pSrcMesh->mNumVertices; ++VertexCounter)
    {
        const auto& Position = pSrcMesh->mVertices[VertexCounter];
        const auto& Normal = pSrcMesh->mNormals[VertexCounter];
        const auto& TexCoord = pSrcMesh->HasTextureCoords(0) ? pSrcMesh->mTextureCoords[0][VertexCounter] : ZeroVector;
        const auto& Tangent = pSrcMesh->HasTangentsAndBitangents() ? pSrcMesh->mTangents[VertexCounter] : ZeroVector;

        OutMesh.Vertices[VertexCounter] = MeshVertex(
            DirectX::XMFLOAT3(Position.x, Position.y, Position.z),
            DirectX::XMFLOAT3(Normal.x, Normal.y, Normal.z),
            DirectX::XMFLOAT2(TexCoord.x, TexCoord.y),
            DirectX::XMFLOAT3(Tangent.x, Tangent.y, Tangent.z)
        );
    }

    // インデックス
    OutMesh.Indices.resize(pSrcMesh->mNumFaces * 3u); // 三角形ポリゴンを前提とする
    for (auto FaceCounter = 0U; FaceCounter < pSrcMesh->mNumFaces; ++FaceCounter)
    {
        const auto& Face = pSrcMesh->mFaces[FaceCounter];
        assert(Face.mNumIndices == 3);

        auto CurrentFaceID = FaceCounter * 3u;
        OutMesh.Indices[CurrentFaceID + 0u] = Face.mIndices[0];
        OutMesh.Indices[CurrentFaceID + 1u] = Face.mIndices[1];
        OutMesh.Indices[CurrentFaceID + 2u] = Face.mIndices[2];
    }

}

// マテリアルデータ解析
void MeshLoader::ParseMaterial(ResMaterial& OutMaterial, const aiMaterial* pSrcMaterial)
{
    if (!pSrcMaterial)
    {
        OutputDebugStringW(L"ソースマテリアルが存在しません\n");
        return;
    }

    // 拡散反射
    {
        aiColor3D Color(0.0f, 0.0, 0.0f);
        if (pSrcMaterial->Get(AI_MATKEY_COLOR_DIFFUSE, Color) == AI_SUCCESS)
        {
            OutMaterial.Diffuse.x = Color.r;
            OutMaterial.Diffuse.y = Color.g;
            OutMaterial.Diffuse.z = Color.b;
        }
        else
        {
            OutMaterial.Diffuse.x = 0.5f;
            OutMaterial.Diffuse.y = 0.5f;
            OutMaterial.Diffuse.z = 0.5f;
        }
    }

    // 鏡面反射
    {
        aiColor3D Specular(0.0f, 0.0f, 0.0f);
        if (pSrcMaterial->Get(AI_MATKEY_COLOR_SPECULAR, Specular) == AI_SUCCESS)
        {
            OutMaterial.Specular.x = Specular.r;
            OutMaterial.Specular.y = Specular.g;
            OutMaterial.Specular.z = Specular.b;
        }
        else
        {
            OutMaterial.Specular.x = 0.5f;
            OutMaterial.Specular.y = 0.5f;
            OutMaterial.Specular.z = 0.5f;
        }
    }

    // 鏡面反射強度
    {
        auto Shininess = 0.0f;
        if (pSrcMaterial->Get(AI_MATKEY_SHININESS, Shininess) == AI_SUCCESS)
        {
            OutMaterial.Shininess = Shininess;
        }
        else
        {
            OutMaterial.Shininess = 0.0f;
        }
    }

    // ディフューズマップ
    {
        aiString MapPath = {};
        if (pSrcMaterial->Get(AI_MATKEY_TEXTURE_DIFFUSE(0), MapPath) == AI_SUCCESS)
        {
            OutMaterial.DiffuseMap = std::string(MapPath.C_Str());
        }
        else
        {
            OutMaterial.DiffuseMap.clear();
        }
    }
}

// メッシュ読み込み
const bool LoadMesh(const wchar_t* MeshFilename, std::vector<ResMesh>& OutMeshes, std::vector<ResMaterial>& OutMaterials)
{
    MeshLoader Loader = {};
    return Loader.Load(MeshFilename, OutMeshes, OutMaterials);
}