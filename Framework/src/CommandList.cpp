#include "CommandList.h"
#include <Logger.h>

CommandList::CommandList()
{

}

CommandList::~CommandList()
{
    Terminate();
}

const bool CommandList::Initialize(
    ID3D12Device* const Device,
    D3D12_COMMAND_LIST_TYPE CmdType,
    uint32_t AllocatorNum)
{
    if (!Device)
    {
        ELOG("CommandList::Initialize >> Device が nullptr.");
        return false;
    }

    // コマンドアロケータの生成
    CmdAllocatorList.resize(AllocatorNum);
    for (auto AllocatorCounter = 0u; AllocatorCounter < AllocatorNum; ++AllocatorCounter)
    {
        auto HR = Device->CreateCommandAllocator(
            CmdType, IID_PPV_ARGS(CmdAllocatorList[AllocatorCounter].GetAddressOf()));

        if (FAILED(HR))
        {
            ELOG("CommandList::Initialize >> コマンドアロケータの生成に失敗.");
            return false;
        }
    }

    // コマンドリストの生成
    auto HR = Device->CreateCommandList(
        1, 
        CmdType,
        CmdAllocatorList[0].Get(),
        nullptr,
        IID_PPV_ARGS(CmdList.GetAddressOf()));

    if (FAILED(HR))
    {
        ELOG("CommandList::Initialize >> コマンドリストの生成に失敗.");
        return false;
    }

    CmdList->Close();

    AllocatorIndex = 0;
    return true;
}

void CommandList::Terminate()
{
    CmdList.Reset();
    CmdList = nullptr;

    for (auto i = 0; i < CmdAllocatorList.size(); ++i)
    {
        CmdAllocatorList[i].Reset();
    }

    CmdAllocatorList.clear();
    CmdAllocatorList.shrink_to_fit();
}

ID3D12GraphicsCommandList* const CommandList::Reset()
{
    auto TargetAllocator = CmdAllocatorList[AllocatorIndex];

    auto HR = TargetAllocator->Reset();
    if (FAILED(HR))
    {
        ELOG("CommandList::Reset >> アロケータのリセットに失敗.");
        return nullptr;
    }

    HR = CmdList->Reset(TargetAllocator.Get(), nullptr);
    if (FAILED(HR))
    {
        ELOG("CommandList::Reset >> コマンドリストのリセットに失敗.");
        return nullptr;
    }

    AllocatorIndex = (AllocatorIndex + 1) & uint32_t(CmdAllocatorList.size());

    return CmdList.Get();
}
